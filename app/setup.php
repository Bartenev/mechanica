<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;

/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_style('sage/main.css', asset_path('styles/main.css'), false, null);
    wp_enqueue_script('sage/main.js', asset_path('scripts/main.js'), ['jquery'], null, true);
    $ajax_params = array(
        'ajax_url' => admin_url('admin-ajax.php'),
    );

    wp_localize_script('sage/main.js', 'admin_url', $ajax_params);
}, 100);

include('ajax-post-grid.php');

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support('soil-clean-up');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-relative-urls');

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'sage'),
        'secondary_navigation' => __('Secondary Navigation', 'sage'),
    ]);

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Use main stylesheet for visual editor
     * @see resources/assets/styles/layouts/_tinymce.scss
     */
    add_editor_style(asset_path('styles/main.css'));
}, 20);

/**
 * Register sidebars
 */
add_action('widgets_init', function () {
    $config = [
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ];
    register_sidebar([
        'name'          => __('Primary', 'sage'),
        'id'            => 'sidebar-primary'
    ] + $config);
    register_sidebar([
        'name'          => __('Footer', 'sage'),
        'id'            => 'sidebar-footer'
    ] + $config);
});

/**
 * Register custom post types
 */
if ( !post_type_exists('case') ) {
	function register_case_study_type() {
		$label_singular = 'Client';
		$label_plural   = 'Clients';
		$taxonomy = 'industry';
		
		// The labels and rewrite for this post type has been 
		// changed to clients. This definition will need to be updated in the 
		// future to prevent any confusion while writing new queries
		
		register_post_type(
			'case',
			array(
				'label'           => $label_plural,
				'description'     => '',
				'public'          => true,
				'taxonomies'      => array( $taxonomy ),
				'show_ui'         => true,
				'show_in_menu'    => true,
				'capability_type' => 'post',
				'hierarchical'    => false,
				'query_var'       => true,
				'has_archive'     => false,
        		'menu_icon'   	  => 'dashicons-screenoptions',
				'rewrite' => array(
					'slug'       => 'clients',
					'with_front' => true,
				),
				'supports' => array(
					'title',
					'revisions',
					'thumbnail',
					'custom-fields',
				),
				'labels' => array (
					'name'               => $label_plural,
					'singular_name'      => $label_singular,
					'menu_name'          => $label_plural,
					'add_new'            => 'Add New',
					'add_new_item'       => 'Add New ' . $label_singular,
					'edit'               => 'Edit',
					'edit_item'          => 'Edit ' . $label_singular,
					'new_item'           => 'New ' . $label_singular,
					'view'               => 'View ' . $label_singular,
					'view_item'          => 'View ' . $label_singular,
					'search_items'       => 'Search ' . $label_plural,
					'not_found'          => 'No ' . $label_plural . ' Found',
					'not_found_in_trash' => 'No ' . $label_plural . ' Found in Trash',
					'parent'             => 'Parent ' . $label_singular,
				)
			)
		);
	}
	add_action('init',  __NAMESPACE__ . '\\register_case_study_type');
}

if ( !post_type_exists('careers') ) {
	function register_careers_type() {
		$label_singular = 'Career';
		$label_plural   = 'Careers';
		$taxonomy = 'careers_category';
		register_post_type(
			'careers',
			array(
				'label'           => $label_plural,
				'description'     => '',
				'public'          => true,
				'taxonomies'      => array( $taxonomy ),
				'show_ui'         => true,
				'show_in_menu'    => true,
				'capability_type' => 'post',
				'hierarchical'    => false,
				'query_var'       => true,
				'has_archive'     => false,
                'menu_icon'   => 'dashicons-universal-access',
				'rewrite' => array(
					'slug'       => 'careers',
					'with_front' => true,
				),
				'supports' => array(
					'title',
					'revisions',
					'thumbnail',
					'custom-fields',
				),
				'labels' => array (
					'name'               => $label_plural,
					'singular_name'      => $label_singular,
					'menu_name'          => $label_plural,
					'add_new'            => 'Add New',
					'add_new_item'       => 'Add New ' . $label_singular,
					'edit'               => 'Edit',
					'edit_item'          => 'Edit ' . $label_singular,
					'new_item'           => 'New ' . $label_singular,
					'view'               => 'View ' . $label_singular,
					'view_item'          => 'View ' . $label_singular,
					'search_items'       => 'Search ' . $label_plural,
					'not_found'          => 'No ' . $label_plural . ' Found',
					'not_found_in_trash' => 'No ' . $label_plural . ' Found in Trash',
					'parent'             => 'Parent ' . $label_singular,
				)
			)
		);
	}
	add_action('init',  __NAMESPACE__ . '\\register_careers_type');
}

if ( !post_type_exists('team_members') ) {
	function register_team_members_type() {
		$label_singular = 'Team Member';
		$label_plural   = 'Team Members';
		// $taxonomy = '';
		register_post_type(
			'team_members',
			array(
				'label'           => $label_plural,
				'description'     => '',
				'public'          => false,
				// 'taxonomies'      => array( $taxonomy ),
				'show_ui'         => true,
				'show_in_menu'    => true,
				'capability_type' => 'post',
				'hierarchical'    => false,
				'query_var'       => true,
				'has_archive'     => true,
                'menu_icon'   => 'dashicons-groups',
				'rewrite' => array(
					'slug'       => 'team_members',
					'with_front' => false,
				),
				'supports' => array(
					'title',
					'revisions',
					'custom-fields',
				),
				'labels' => array (
					'name'               => $label_plural,
					'singular_name'      => $label_singular,
					'menu_name'          => $label_plural,
					'add_new'            => 'Add New',
					'add_new_item'       => 'Add New ' . $label_singular,
					'edit'               => 'Edit',
					'edit_item'          => 'Edit ' . $label_singular,
					'new_item'           => 'New ' . $label_singular,
					'view'               => 'View ' . $label_singular,
					'view_item'          => 'View ' . $label_singular,
					'search_items'       => 'Search ' . $label_plural,
					'not_found'          => 'No ' . $label_plural . ' Found',
					'not_found_in_trash' => 'No ' . $label_plural . ' Found in Trash',
					'parent'             => 'Parent ' . $label_singular,
				)
			)
		);
	}
	add_action('init',  __NAMESPACE__ . '\\register_team_members_type');
}

if ( !post_type_exists('challenges') ) {
	function register_challenges_type() {
		$label_singular = 'Challenge';
		$label_plural   = 'Challenges';
		// $taxonomy = '';
		register_post_type(
			'challenges',
			array(
				'label'           => $label_plural,
				'description'     => '',
				'public'          => true,
				// 'taxonomies'      => array( $taxonomy ),
				'show_ui'         => true,
				'show_in_menu'    => true,
				'capability_type' => 'post',
				'hierarchical'    => false,
				'query_var'       => true,
				'has_archive'     => false,
        		'menu_icon'   => 'dashicons-image-filter',
				'rewrite' => array(
					'slug'       => 'challenges',
					'with_front' => true,
				),
				'supports' => array(
					'title',
					'revisions',
					'custom-fields',
				),
				'labels' => array (
					'name'               => $label_plural,
					'singular_name'      => $label_singular,
					'menu_name'          => $label_plural,
					'add_new'            => 'Add New',
					'add_new_item'       => 'Add New ' . $label_singular,
					'edit'               => 'Edit',
					'edit_item'          => 'Edit ' . $label_singular,
					'new_item'           => 'New ' . $label_singular,
					'view'               => 'View ' . $label_singular,
					'view_item'          => 'View ' . $label_singular,
					'search_items'       => 'Search ' . $label_plural,
					'not_found'          => 'No ' . $label_plural . ' Found',
					'not_found_in_trash' => 'No ' . $label_plural . ' Found in Trash',
					'parent'             => 'Parent ' . $label_singular,
				)
			)
		);
	}
	add_action('init',  __NAMESPACE__ . '\\register_challenges_type', 0);
}

/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action('the_post', function ($post) {
    sage('blade')->share('post', $post);
});

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Add JsonManifest to Sage container
     */
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    /**
     * Add Blade to Sage container
     */
    sage()->singleton('sage.blade', function (Container $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view']);
    });

    /**
     * Create @asset() Blade directive
     */
    sage('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
    });
});

if (function_exists('acf_add_options_page')) {

	$args = array(
		'page_title' => 'Theme Settings',
		'menu_title' => 'Theme Settings',
		'position' => 3,
		'icon_url' => 'dashicons-admin-site',
	);
	acf_add_options_page( $args );
}

/*
 * Gravity Forms Settings
 */

add_filter('gform_init_scripts_footer', '__return_true');

add_filter( 'gform_cdata_open', __NAMESPACE__ . '\\wrap_gform_cdata_open' );
function wrap_gform_cdata_open( $content = '' ) {
	$content = 'document.addEventListener( "DOMContentLoaded", function() { ';
	return $content;
}
add_filter( 'gform_cdata_close', __NAMESPACE__ . '\\wrap_gform_cdata_close' );
function wrap_gform_cdata_close( $content = '' ) {
	$content = ' }, false );';
	return $content;
}

add_filter( 'gform_ajax_spinner_url',  __NAMESPACE__ . '\\tgm_io_custom_gforms_spinner' );
function tgm_io_custom_gforms_spinner( $src ) {
	return get_stylesheet_directory_uri() . '/assets/images/ajax-loader.gif';
}

// filter the Gravity Forms button type
add_filter( 'gform_submit_button', __NAMESPACE__ . '\\form_submit_button', 10, 2 );
function form_submit_button( $button, $form ) {
	return "<button class='c-button' id='gform_submit_button_{$form['id']}'><span>Submit</span></button>";
}
// add custom input type file
add_filter( 'gform_field_input', __NAMESPACE__ . '\\my_custom_function', 10, 5 );
function my_custom_function( $input, $field, $value, $lead_id, $form_id ) {
	if ( $field->type == 'fileupload' ) {
		$size = !empty($field->maxFileSize) ? $field->maxFileSize * 1024 * 1024 : 33554432;
		$input = '<div class="ginput_container ginput_container_fileupload" data-file-upload>
					<input type="hidden" name="MAX_FILE_SIZE" value="' . $size .'">
					<input name="input_' . $field->id . '" id="input_' . $form_id . '_' . $field->id . '" type="file" class="' . $field->size .' " aria-describedby="extensions_message_' . $form_id . '_' . $field->id . '" onchange="javascript:gformValidateFileSize( this, ' . $size .' );">
					<input name="input_' . $field->id . '_add" id="input_' . $form_id . '_' . $field->id . '_add" type="text" placeholder="Select Files to Upload" readonly>
					<a class="c-button"><span>Upload</span></a>
					<span id="extensions_message_1_6" class="screen-reader-text"></span>
					<div class="validation_message">' . $field->errorMessage . '</div>
				  </div>';
	}
	return $input;
}

/*
 * End Gravity Forms Settings
 */

//shortcode Copyright and current year
function get_copyright($atts) {
	return 'Copyright ' . date('Y') . '. ';
}

add_shortcode('copyright', __NAMESPACE__ . '\\get_copyright');

// add support svg image
function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes',  __NAMESPACE__ . '\\cc_mime_types');


function redirect_search_query( $query, $error = true ) {
    if ( is_search() ) {
		$query->is_search = false;        
		$query->is_404 = true;            
    }
}

add_action( 'parse_query', __NAMESPACE__ . '\\redirect_search_query' );