<?php

namespace App\Controllers;

use WP_Query;
use Sober\Controller\Controller;

class BlogPage extends Controller {

	protected $acf = true;

	public $featured_blog_post_is = '';

	// get featured blog post
	public function featuredBlogPost() {
		$options = get_field('options');
		$featured_data = [];

		$args_featured = array(
			'post_type' => 'post',
			'post_date' => 'DESC',
			'posts_per_page' => 1
		);

		if ( $options == 'manually_select' ) {
			$args_featured['p'] = get_field('single_post')[0];
		}

		if ( $options == 'most_recent_in_category' ) {
			$args_featured['cat'] = get_field('post_categories');
		}

		$query = new WP_Query( $args_featured );

		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) {
				$query->the_post();
				$category = get_the_category(get_the_id());
				$categories_str = '';

				foreach ($category as $key => $cat) {
					$categories_str .=  $key !== count($category) - 1 ? $cat->name . ', ' : $cat->name;
				}

				$featured_data = array(
					'image' => App::getImageSrc(get_post_thumbnail_id()),
					'category' => $categories_str,
					'id' => get_the_id(),
					'title' => get_the_title(),
					'author' => get_the_author(),
					'url' => get_the_permalink(),
					'excerpt' => get_the_excerpt()
				);
			}

			$this->public = $featured_data['id'];
		}
		wp_reset_query();

		return $featured_data;
	}


	// get post categories data
	public function newsCategory() {
		$terms = get_terms('category');
		$terms_data = [];

		foreach ( $terms as $term ) {
			$term_data = array(
				'cat_name' => $term->name,
				'cat_slug' => $term->slug,
			);

			array_push($terms_data, $term_data);
		}


		return $terms_data;
	}

	// get post data categories
	public function newsPosts() {
		$args = array(
			'post_type' => 'post',
			'post__not_in' => array($this->public),
			'post_date' => 'DESC',
			'posts_per_page' => 8
		);

		$query = new WP_Query( $args );
		$query_count = $query->post_count;
		$query_date = [];
		$count = 1;

		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) {
				$query->the_post();
				$size = '';
				$category = get_the_category(get_the_id());
				$categories_str = '';

				switch(true) {
					case ( $query_count <= 4 and $query_count != 3 and $query_count != 1 ) :
						$size = 6;
						break;
					case ( $count == 3 and $query_count == 3 || $query_count == 5 ) :
						$size = 12;
						break;
					case ( $count == 1 and $query_count >= 3 ) :
						$size = 6;
						break;
					case ( $count == 2 and $query_count >= 3 ) :
						$size = 6;
						break;
					case ( $query_count == 5 and $count == 4 || $count == 5 ) :
						$size = 6;
						break;
					case ( $count == 4 and $query_count >= 6 ) :
						$size = 4;
						break;
					case ( $count == 5 and $query_count >= 6 ) :
						$size = 4;
						break;
					case ( $count == 6 || $count == 7 and $query_count >= 6 ) :
						$size = 4;
						break;
					default :
						$size = 12;
				}

				foreach ($category as $key => $cat) {
					$categories_str .=  $key !== count($category) - 1 ? $cat->name . ', ' : $cat->name;
				}

				$post_data = array(
					'id' => get_the_id(),
					'title' => get_the_title(),
					'author' => get_the_author(),
					'url' => get_the_permalink(),
					'category' => $categories_str,
					'image' => App::getImageSrc(get_post_thumbnail_id()),
					'count' => $count,
					'size' => $size
				);
				$count++;

				array_push($query_date, $post_data);
			}
		}
		wp_reset_query();

		return $news_data = array(
			'max_num_pages'=> $query->max_num_pages,
			'query_date' => $query_date
		);
	}
}
