<?php
// https://github.com/soberwp/controller
namespace App\Controllers;

use Sober\Controller\Controller;
class App extends Controller
{


    public function siteName()
    {
        return get_bloginfo('name');
    }
    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }
    public static function getImageSrc($image_id, $size = 'full')
    {
        return wp_get_attachment_image_src($image_id, $size)[0];
    }

    public static function getImageAlt($image_id) {
      $title = get_post_meta($image_id, '_wp_attachment_image_alt', true);
      if (!strlen($title)) {
        $title = get_the_title($image_id);
      }
      return $title;
    }

}
