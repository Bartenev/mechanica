<?php

add_action( 'wp_ajax_post_grid', 'post_grid_callback' );
add_action( 'wp_ajax_nopriv_post_grid', 'post_grid_callback' );

function post_grid_callback() {
	$type = $_POST['postType'];
	$page = $_POST['page'];
	$exclude_post = $_POST['excludePost'];
	$category = implode(',', $_POST['category']);
	$sub_category = $_POST['subCat'];
	$html = '';
	$html_sub_filter = '';

	// get post
	if ( !empty($type) ) {
		$args = array (
			'post_type' => $type,
			'orderby' => 'menu_order',
        	'order' => 'ASC',
			'posts_per_page' => 8,
			'paged' => $page,
			'post_status' => 'publish'
		);

		if ( !empty($category) && $category != '*' && $type == 'post' ) {
			$args['category_name'] = $category;
		}

		if ( $type == 'post' ) {
			$args['orderby'] = '';
			$args['order'] = '';
			$args['post_date'] = 'DESC';
		}

		if ( $type != 'post' && !empty($category) && $category != '*' ) {
			$args['tax_query'][] = array(
				'taxonomy' => 'careers_category',
				'field' => 'slug',
				'terms' => explode(',', $category),
			);
		}

		if ( !empty($exclude_post) ) {
			$args['post__not_in'] = array($exclude_post);
		}

		if ( $type == 'featured' ) {
			$args['post_type'] = array('case');
			$args['meta_query'][] = array(
				'key' => 'featured_post',
				'value' => true,
				'compare' => '='
			);
		}

		if ( $type == 'industry' ) {
			$industry_categories = get_terms('industry');
			$category_array = array();

			foreach ($industry_categories as $category) {
				array_push($category_array, $category->slug);
			}

			$args['post_type'] = array('case');
			$args['tax_query'] = array(
				'relation' => 'OR',
				array(
					'taxonomy' => 'industry',
					'field'    => 'slug',
					'terms'    => $category_array,
				)
			);

			if (!empty($sub_category)) {
				$args['tax_query'] = array(
					'relation' => 'OR',
					array(
						'taxonomy' => 'industry',
						'field'    => 'slug',
						'terms'    => $sub_category,
					)
				);
			}
		}

		if ( $type == 'challenges' ) {
			$args['post_type'] = 'challenges';
		}

		if ( $type == 'careers' ) {
			$args['posts_per_page'] = 12;
		}

		if ( $type == 'team_members' ) {
			$args['posts_per_page'] = 9;
		}

		$query = new WP_Query( $args );
		$query_count = count($query->posts);
		$count = 1;
		$count_animation = 0;

		if ( $query->have_posts() ) {

			if ($type == 'industry') {
				$industry_categories = get_terms('industry');
				$html_sub_filter .= '<div data-waypoint><ul data-slide-up>';

				foreach ($industry_categories as $key => $category) {
					$line = $key != 0 ? '<i>|</i>' : '';
					$active = $sub_category == $category->slug ? 'class="active"' : '';
					$html_sub_filter .= '<li data-duration-0' . $key . 's>' . $line .' <span ' . $active . ' data-filter="' . $category->slug .'">' . strtoupper($category->name) . '</span></li>';
				}

				$html_sub_filter .= '</ul></div>';
			}

			while( $query->have_posts()) {
				$query->the_post();

				$image = App::getImageSrc(get_post_thumbnail_id());

				if ( $type == 'post' ) {

					$size = '';
					$category = get_the_category(get_the_id());
					$categories_str = '';

					switch(true) {
						case ( $query_count <= 4 and $query_count != 3 and $query_count != 1 ) :
							$size = 6;
							break;
						case ( $count == 3 and $query_count == 3 || $query_count == 5 ) :
							$size = 12;
							break;
						case ( $count == 1 and $query_count >= 3 ) :
							$size = 6;
							break;
						case ( $count == 2 and $query_count >= 3 ) :
							$size = 6;
							break;
						case ( $query_count == 5 and $count == 4 || $count == 5 ) :
							$size = 6;
							break;
						case ( $count == 4 and $query_count >= 6 ) :
							$size = 4;
							break;
						case ( $count == 5 and $query_count >= 6 ) :
							$size = 4;
							break;
						case ( $count == 6 || $count == 7 and $query_count >= 6 ) :
							$size = 4;
							break;
						default :
							$size = 12;
					}

					foreach ($category as $key => $cat) {
						$categories_str .=  $key !== count($category) - 1 ? $cat->name . ', ' : $cat->name;
					}
					if ($categories_str!=='Press') { $author_str = '<span>By ' . get_the_author() . '</span>'; } else { $author_str = ''; }

					if ( $query->post_count >= 7 && $count == 6 || $count == 7 ) {

						if ( $count == 6 ) { $html .= '<div class="col-xs-12 col-sm-4" data-waypoint>'; }
						$margin = $count == 6 ? 'm-b-12' : '';

						$html .= '<a href="' . get_the_permalink() . '" class="row" data-slide-up>
                                <div class="col-xs-12" data-duration-02s>
                                    <div class="c-featured-post c-featured-post_medium ' . $margin . '">
                                        <div class="c-featured-post__background ' . strtolower($categories_str) . '" style="background-image: url(' . $image . ');"></div>
                                        <div class="c-featured-post__content">
                                            <h2>
			                                    <span>' . $categories_str . '</span>
			                                    ' . get_the_title() . '
			                                </h2>'. $author_str .'
                                        </div>
                                    </div>
                                </div>
                            </a>';

						if ( $count == 7 ) { $html .= '</div>'; }
					}else {
						$animation_duration = 'data-duration-0s';
						if ( $count == 2 || ($count == 4 && $query_count == 4) || ($count == 5 && $query_count >= 5) ) $animation_duration = 'data-duration-01s';

						$html .= '<a href="' . get_the_permalink() . '" class="col-xs-12 col-sm-' . $size . '" data-waypoint ' . $count . '>
		                        <div class="c-featured-post" data-slide-up>
		                            <div class="c-featured-post__background ' . strtolower($categories_str) . '"' . $animation_duration . ' style="background-image: url(' . $image . ');"></div>
		                            <div class="c-featured-post__content" ' . $animation_duration . '>
		                                <h2>
		                                    <span>' . $categories_str . '</span>
		                                    ' . get_the_title() . '
		                                </h2>'. $author_str .'
		                            </div>
		                        </div>
		                    </a>';
					}
					$count++;
				}

				if ( $type == 'careers' ) {
					if ( !empty( get_post_thumbnail_id()) ) {
						$image = '<div class="c-career-post__background" style="background-image: url(' . $image . ')"></div>';
					}else {
						$image = '<div class="c-career-post__background"></div>';
					}
					$duration = $count_animation == 0 ? 'data-duration-' . $count_animation . 's' : 'data-duration-0' . $count_animation . 's';

					$html .= '<a href="' . get_the_permalink() . '" class="col-xs-12 col-sm-3" data-waypoint>
			                    <div data-slide-up>
				                    <div class="c-career-post" ' . $duration . '>
				                        ' . $image . '
				                        <h2>' . get_the_title() . '</h2>
				                    </div>
								</div>
			                </a>';

					$count++;
					$count_animation == 3 ? $count_animation = 0 : $count_animation++;
				}

				if ( $type == 'featured' || $type == 'industry' || $type == 'challenges' ) {
					$id = get_the_id();
					$post_excerpt = get_post_meta($id, 'excerpt')[0];
					if ( strlen($post_excerpt) > 222 ) {
						$post_excerpt = substr($post_excerpt, 0, 198) . '...';
					}
					$bg_image_id = get_post_type() == 'case' ? get_post_meta($id, 'image')[0] : get_post_meta($id, 'background_image')[0];
					$bg_image = 'background-image: url(' . App::getImageSrc($bg_image_id) . ');';
					$bg_type = get_post_meta($id, 'background_type')[0];
					$bg_color = $bg_type == 'color' || $type === 'challenges' ? 'data-color-' . get_post_meta($id, 'background_color')[0] : '';
					$logo = get_post_meta($id, 'select_type')[0]  == 'image' ? App::getImageSrc(get_post_meta($id, 'logo_image_grid')[0]) : '';
					$text = get_post_meta($id, 'select_type')[0]  == 'text' ? get_post_meta($id, 'text')[0] : '';
					$select_type = get_post_meta($id, 'select_type')[0] == 'image' ? 'style="background-image: url( ' . $logo .' )"' : 'data-text';
					$bg = '';
					$duration = $count_animation == 0 ? 'data-duration-' . $count_animation . 's' : 'data-duration-0' . $count_animation . 's';
					$button_text = $type == 'challenges' ? 'Learn More' : 'SEE THE WORK';

					if ($type !== 'challenges' && $bg_type == 'image') {
						$bg .= '<svg class="c-filterable-post__image-blur">
	                                <defs>
	                                    <filter id="blur-effect-' . $count . '" class="blur-effect">
	                                        <feGaussianBlur stdDeviation="2"/>
	                                    </filter>
	                                </defs>
	                                <image x="0" y="0" class="svg-image" xlink:href="' . App::getImageSrc($bg_image_id) . '"/>
	                                <image x="0" y="0" class="svg-image svg-image_blur"
	                                       xlink:href="' . App::getImageSrc($bg_image_id) . '"
	                                       filter="url(#blur-effect-' . $count . ')"/>
	                            </svg>
	                            <div class="c-filterable-post__image" style="background-image: url(' . App::getImageSrc($bg_image_id) . ');"></div>
	                            <image class="c-filterable-post__image-size" src="' . App::getImageSrc($bg_image_id) . '" />';
					}else {
						$secondary_background_image = App::getImageSrc(get_post_meta($id, 'secondary_background_image')[0]);
						$bg .= '<div class="c-filterable-post__background" style="' . $bg_image . '"></div>
	                            <div class="c-filterable-post__background-hover" style="background-image: url(' . $secondary_background_image . ');"></div>';
					}

					$href = '<a class="c-button" href="' . get_the_permalink() . '"><span>' . $button_text . '</span></a>';
					$html .= '<div class="l-4-col__item" data-waypoint>
			                    <div class="c-filterable-post" data-type="' . $type . '" ' . $bg_color . ' data-slide-up>
			                        <div class="c-filterable-post__inner" ' . $duration . ' style="' . $bg_image . '">
				                        <div class="c-filterable-post__logo" ' . $select_type . '><span>' . $text . '</span></div>
				                        <div class="c-filterable-post__content">';
											
										if ( $post_excerpt ) {
											$html .= '<p>' . $post_excerpt . '</p>';
										}
										
					$html .=			$href .'
				                        </div>
				                        ' . $bg . '
									</div>
			                    </div>
			                </div>';

					$count++;
					$count_animation == 3 ? $count_animation = 0 : $count_animation++;
				}

				if ( $type == 'team_members' ) {
					$id = get_the_id();
					$professional_title = get_post_meta($id, 'professional_title')[0];
					// $post_content = get_post_meta($id, 'content', true);
					$post_content = get_field('content', $id);
					$featured_image = App::getImageSrc(get_post_meta($id, 'image')[0], 'medium_large');
					$title = !empty(get_post_meta($id, 'first_name')[0]) && !empty(get_post_meta($id, 'last_name')[0]) ? get_post_meta($id, 'first_name')[0] . ' ' . get_post_meta($id, 'last_name')[0] : get_post_title();
					$first_name = !empty(get_post_meta($id, 'first_name')[0]) ? get_post_meta($id, 'first_name')[0] : get_post_title();
					$email = get_post_meta($id, 'contact_email')[0];
					$phone = get_post_meta($id, 'contact_phone')[0];
					$posts = get_post_meta($id, 'posts')[0];
					$image_position = get_post_meta($id, 'image_position')[0];

					$posts_html = '';
					$duration = $count_animation == 0 ? 'data-duration-' . $count_animation . 's' : 'data-duration-0' . $count_animation . 's';

					if ( !empty($posts) ) {
						$posts_html .= '<ul class="c-team-row__post"><li>Recent thoughts from ' . $first_name . '</li>';
						foreach ($posts as $post) {
							$posts_html .= '<li><a href="' . get_the_permalink($post->ID) . '">' . $post->post_title . '</a></li>';
						}
						$posts_html .= '</ul>';
					}

					$html .= '<div class="col-xs-12 col-sm-4" data-waypoint>
			                    <div data-slide-up>
				                    <div class="c-team-row c-team-row_' . $image_position . '" ' . $duration . '
				                        data-swipe-panel-trigger
				                        style="background-image: url(' . $featured_image . ');"
				                        data-image="' . $featured_image . '">
				                        <span class="c-team-row__arrow">
				                            <svg xmlns="http://www.w3.org/2000/svg"
				                                 xmlns:xlink="http://www.w3.org/1999/xlink"
				                                 width="24px" height="31px">
				                                <path fill-rule="evenodd"  fill="rgb(255, 255, 255)" d="M0.506,30.094 L16.054,15.098 L0.506,0.099 L7.944,0.099 L23.494,15.098 L7.944,30.094 L0.506,30.094 Z"/>
				                            </svg>
				                        </span>
				                        <div class="c-team-row__info" data-first-name="' . $first_name . '" data-email="' . $email . '" data-phone="' . $phone . '">
				                            <h2>
				                                ' . $title .'
				                                <span>' .$professional_title . '</span>
				                            </h2>
				                            <div class="c-team-row__content">' . $post_content . '</div>
				                            ' . $posts_html . '
				                        </div>
				                    </div>
								</div>
			                </div>';

					$count_animation == 2 ? $count_animation = 0 : $count_animation++;
				}
			}
		}
	}

	if ( $html != '' ) {
		echo  json_encode($data = array(
			'html' => $html,
			'maxPage' => $query->max_num_pages,
			'htmlSubFilter' => $html_sub_filter
		));
		die();
	}

	echo json_encode($data = array('html'=>''));
	die();
}
?>
