class SwipePanel {
    constructor(trigger) {
        this.trigger = trigger;
        this.id = 'swipe-panel-'+Math.random();

        this.init();
    }
    init() {
        let wrap = document.createElement('div');
        let contentWrap = document.createElement('div');
        let content = document.createElement('div');
        let btnClose = document.createElement('div');

        wrap.classList.add('l-swipe-panel');
        wrap.id = this.id;
        contentWrap.classList.add('l-swipe-panel__wrap');
        content.classList.add('l-swipe-panel__content');
        btnClose.classList.add('l-swipe-panel__close');

        btnClose.innerHTML = 'close';

        contentWrap.appendChild(btnClose);
        contentWrap.appendChild(content);
        wrap.appendChild(contentWrap);

        document.body.appendChild(wrap);

        this.closeTrigger = btnClose;
        this.wrapper = wrap;
        this.container = content;

        this.listenEvents();
    }
    listenEvents() {
        this.closeTrigger.addEventListener('click', function () {
            this.close();
        }.bind(this));

        $(window).on('click', function (e) {
            let content = $('.l-swipe-panel__content');

            if ( this.isOpen && !content.is(e.target) && content.has(e.target).length === 0 && !$('.c-team-row ').is(e.target)) {
                this.close();
            }

        }.bind(this));

        window.addEventListener('keyup', function (e) {
            if (e.keyCode === 27) {
                this.close();
            }
        }.bind(this));
    }
    open(content) {
        this.container.innerHTML = content;
        document.body.dataset.swipePanelOpen = this.id;
        document.getElementById(this.id).classList.add('is-visible');
        this.isOpen = true;
    }
    close() {
        document.body.removeAttribute('data-swipe-panel-open');
        document.getElementById(this.id).classList.remove('is-visible');
        this.isOpen = false;
    }
}

export default SwipePanel;