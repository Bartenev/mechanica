import Swiper from 'swiper/dist/js/swiper.js';

let swiper = {
    swipersArray: [],
    swaperWrap: document.getElementsByClassName('c-media-swiper'),
    init() {

        for (let i = 0; i < this.swaperWrap.length; i++) {
            let s;

            if ( this.swaperWrap[i].dataset.slider === 'media-slider' ) {

                s = new this.swiperConstructior(this.swaperWrap[i], {
                    wrapperClass: 'c-media-swiper__inner',
                    slideClass: 'c-media-swiper__slide',
                    touchRatio: 1,
                    navigation: {
                        nextEl: this.swaperWrap[i].getElementsByClassName('c-media-swiper__button_next'),
                        prevEl: this.swaperWrap[i].getElementsByClassName('c-media-swiper__button_prev'),
                    },
                    pagination: {
                        el: this.swaperWrap[i].getElementsByClassName('c-media-swiper__pagination'),
                        bulletElement: 'li',
                        clickable: true,
                    },
                    on: {
                       init: function () {
                           $('.c-media-swiper__slide').on('click', function (e) {
                                console.log( e.target );
                           });
                       },
                    },
                });
            }

            this.swipersArray.push(s);
        }
    },
    swiperConstructior(container, options) {
        this.container = container;
        this.settings = {
            loop: true,
            speed: 1000,
            grabCursor: true,
        };
        if ($(this.container).find('.'+options.slideClass).length > 1) {
          this.slider = new Swiper( this.container, $.extend(this.settings, options) );
        }
    },
};

export default swiper;
