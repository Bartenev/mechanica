require('spectragram');

let instagram = {
    instaToken: $('.c-instagram__grid').data('accetss-token'),
    instaID: $('.c-instagram__grid').data('client-id'),
    instaWrap: $('.c-instagram__grid'),
    count: 20,
    init() {
        if ( this.instaWrap.length ) {
            let widthPercent = (this.instaWrap.width() * 0.1 - 10) * 100 / this.instaWrap.width(),
                widthPixel = this.instaWrap.width() / 10 - 10;

            $.fn.spectragram.accessData = {
                accessToken: this.instaToken,
                clientID: this.instaID,
            };

            if ( window.innerWidth <= 768 ) {
                this.count = 4;

                widthPercent = (this.instaWrap.width() * 0.5 - 10) * 100 / this.instaWrap.width();
                widthPixel = this.instaWrap.width() / 2 - 10;
            }

            this.instaWrap.spectragram('getUserFeed', {
                max: this.count,
                size: 'big',
                wrapEachWith: '<li class="c-instagram__item" style="flex:0 1 ' + widthPercent + '%;height:' + widthPixel + 'px;">',
            });

            this.event();
        }
    },
    event() {
        $(window).on('resize', function () {
            this.resizeIntargarm();
        }.bind(this));
    },
    resizeIntargarm() {
        let widthPercent = (this.instaWrap.width() * 0.1 - 10) * 100 / this.instaWrap.width(),
            widthPixel = this.instaWrap.width() / 10 - 10;

        if ( window.innerWidth <= 768 ) {
            widthPercent = (this.instaWrap.width() * 0.5 - 10) * 100 / this.instaWrap.width();
            widthPixel = this.instaWrap.width() / 2 - 10;
        }

        this.instaWrap.find('.c-instagram__item').css({
            'flex' : '0 1 ' + widthPercent + '%',
            'height' :  widthPixel + 'px',
        });
    },
};

export default instagram;