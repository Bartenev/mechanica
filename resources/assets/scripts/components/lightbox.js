import Plyr from 'plyr/dist/plyr.polyfilled';

class Lightbox {
    constructor(trigger, options) {

        this.trigger = trigger;
        this.options = options;
        this.isOpen = false;

        this.id = 'lbox-id-'+Math.random();

        this.initializeLayout();

    }

    initializeLayout() {
        this.trigger.dataset.lightboxTrigger = this.id;

        let wrap = document.createElement('div');
        let content = document.createElement('div');
        let btnX = document.createElement('div');


        wrap.classList.add('l-lightbox');
        wrap.id = this.id;
        content.classList.add('l-lightbox__content');
        btnX.classList.add('l-lightbox__x');
        content.appendChild(btnX);
        wrap.appendChild(content);

        document.body.appendChild(wrap);

        this.closeTrigger = btnX;
        this.wrapper = wrap;
        this.container = content;

        this.listenEvents();
    }

    listenEvents() {
        this.trigger.addEventListener('click', function() {
            this.open();
            console.log( 'click' );
        }.bind(this));

        this.closeTrigger.addEventListener('click', function () {
            this.close();
            this.isOpen = false;
        }.bind(this));

        window.addEventListener('keyup', function (e) {
            if (e.keyCode === 27 && this.isOpen) {
                this.close();
            }
        }.bind(this));
    }

    open() {
        document.body.dataset.lightboxOpen = this.id;
        document.getElementById(this.id).classList.add('is-visible');
        this.isOpen = true;
    }

    close() {
        document.body.removeAttribute('data-lightbox-open');
        document.getElementById(this.id).classList.remove('is-visible');
        this.isOpen = false;
    }
}

class MediaLightbox extends Lightbox {
    constructor (trigger) {
        super(trigger);
        this.mediaSrc = trigger.dataset.lightboxMedia;
        this.setUpPlayer();
    }
    setUpPlayer() {
        const playerContainer = document.createElement('div');
        const playerId = 'player-id-'+ Math.random().toFixed(4);

        if( this.mediaSrc.indexOf('youtube') > 0 ) {
            playerContainer.dataset.plyrProvider = 'youtube';
        }
        else if( this.mediaSrc.indexOf('vimeo') > 0 ) {
            playerContainer.dataset.plyrProvider = 'vimeo';
        }
        else {
            if( isNaN(this.mediaSrc) ) {
                playerContainer.dataset.plyrProvider = 'youtube';
            }
            else {
                playerContainer.dataset.plyrProvider = 'vimeo';
            }
        }

        playerContainer.dataset.plyrEmbedId = this.mediaSrc + '?origin='+ document.location.origin;
        playerContainer.id = playerId;

        this.container.appendChild(playerContainer);

        this.player = new Plyr(playerContainer, {
            // debug: true,
            autoplay: false,
        });
        console.log( this.player );
    }
    open() {
        super.open();
        this.player.play();
    }
    close() {
        super.close();
        this.player.pause();
    }
}

export default {
    init() {
        let triggers = document.querySelectorAll('[data-lightbox-trigger]');
        for (let i = 0; i < triggers.length; i++) {
            let el  = triggers[i];
            if( el.dataset.lightboxMedia ) {
                new MediaLightbox(el);
            }
            else {
                new Lightbox(el);
            }
        }
    },
    make(target, dataType) {
        if(dataType == 'media') {
            new MediaLightbox(target);
        }
        else {
            return new Lightbox(target);
        }
    },
}
