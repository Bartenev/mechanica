import waypoints from './waypoints';
import teamMemberInfo from './team-member-info'


let postGrid = {
    container: $('[data-container-ajax]'),
    subFilter: $('.c-sub-filter-bar'),
    loadStatus: $('.с-loader-ellipse'),
    ajax: true,
    data: {
        action: 'post_grid',
        page: 2,
    },
    init() {
        if ( this.container.length ) {
            this.event();
            this.data.postType = postGrid.container.data('post-type');
            this.data.excludePost = postGrid.container.data('exclude-post');
        }

        this.sizeGrid();

        this.changeUrl(this.data, 'load');
    },
    sizeGrid() {
        if (this.container.data('post-type') !== 'post') {
            this.container.find('[data-slide-up]').each(function (index, el) {
                if ( $(el).parent().is('.c-sub-filter-bar') ) return;

                let width = $(el).width(),
                    blurSVG = $(el).find('.svg-image'),
                    imageSize = $(el).find('.c-filterable-post__image-size'),
                    positionImage = '';


                if ( window.innerWidth >= 768 ) {
                    $(el).height(width);
                }else {
                    $(el).removeAttr('style');
                }

                if ( $('body').is('.is-ie') ) {
                    blurSVG.attr('width', imageSize.width()).attr('height', imageSize.height());
                }

                if ( imageSize.width() > $(el).width() ) {
                    positionImage = imageSize.offset().left - $(el).offset().left;

                    blurSVG.attr('x', positionImage);
                }else {
                    blurSVG.attr('x', 0);
                }
            });
        }else {
            let height = 0,
                smallItemHeight = 0;

            this.container.find('.c-featured-post_medium').each(function (index, el) {
                let contentHeight = $(el).find('.c-featured-post__content').innerHeight();

                if ( contentHeight > smallItemHeight ) smallItemHeight = contentHeight + 10;
            });

            this.container.find('.c-featured-post').each(function (index, el) {
                let smallItem = $(el).is('.c-featured-post_medium'),
                    contentHeight = $(el).find('.c-featured-post__content').innerHeight();

                if ( !smallItem && contentHeight > height ) {
                    height = contentHeight;
                }
            });

            if ( window.innerWidth >= 768 ) {
                if ( height > smallItemHeight + 280 ) {
                    $('.c-featured-post').height(height);
                    $('.c-featured-post_medium').height(height / 2 - 6);
                }else {
                    $('.c-featured-post').height(smallItemHeight * 2 + 12);
                    $('.c-featured-post_medium').height(smallItemHeight);
                }
            }
        }

        // get careers post background
        if ( this.container.data('post-type') === 'careers' ) {
            let count = 1, row = 1;
            this.container.find('[data-slide-up]').each(function (index, el) {
                if ( count % 2 === 0 && row % 2 !== 0 ) {
                    $(el).find('.c-career-post__background').addClass('c-career-post__background_default');
                }
                if ( count % 2 !== 0 && row % 2 === 0 ) {
                    $(el).find('.c-career-post__background').addClass('c-career-post__background_default');
                }

                if ( count === 4 ) {row++; count = 0;}
                count++;
            });
        }
    },
    event() {
        $(document).on('click', '.c-filter-bar span', function (e) {
            this.onFilter(e.target);

        }.bind(this));

        $(document).on('click', '.c-filter-bar', function (e) {
            if  ( window.innerWidth <= 768 ) {
                this.filterDropDown(e.target);
            }

        }.bind(this));

        window.addEventListener('scroll', function() {
            this.onScroll();
        }.bind(this));

        $(window).on('resize', function () {
            this.sizeGrid();
            waypoints.reInit($('[data-waypoint]'));
        }.bind(this));

        //sub category
        $(document).on('click', '.c-sub-filter-bar span', function (e) {
            this.data.postType = 'industry';
            this.data.subCat = $(e.target).data('filter');
            this.data.page = 1;

            $(e.target).parent().siblings().find('span')
                .add('[data-filter-type]')
                .removeClass('active');
            $(e.target)
                .add('[data-filter-type="industry"]')
                .addClass('active');

            this.onAjax('onFilter');
        }.bind(this));

        $('.l-wrap').on('click', '.c-filterable-post', function() {
          if ($(this).find('a').length===1) {
            let link = $(this).find('a').prop('href');
            document.location.href = link;
          }
        }).bind(this);
    },
    onFilter(target) {
        let _target = $(target),
            parent = _target.parent();

        if ( _target.data('filter') !== undefined && _target.data('filter-type') === undefined &&  _target.parents('.c-sub-filter-bar').length === 0) {
            let dataFilter = _target[0].dataset.filter;

            this.data.page = 1;

            parent.parent().removeClass('open').removeAttr('style');
            parent.parent().find('li:eq(0) span').text(_target.text());

            if ( dataFilter === '*' ) {
                this.data.category = [dataFilter];
                _target.addClass('active').parent().siblings().find('span').removeClass('active');

                this.onAjax('onFilter');
                window.location.hash = `#post_type=${this.data.postType}&cat=${this.data.category}`;
                this.data.category = [];
                return;
            }


            if ( this.data.category === undefined && !_target.is('.active') ) {
                this.data.category = [dataFilter];
            }else if ( !_target.is('.active') ) {

                this.data.category.push(dataFilter);
                let cat = [];

                for (let i in this.data.category) {
                    if ( this.data.category[i] !== '*' ) {
                        cat.push(this.data.category[i]);
                    }
                }

                this.data.category = cat;
            }else {
                for ( let i = 0; i < this.data.category.length; i++ ) {
                    if ( this.data.category[i] === dataFilter ) {
                        this.data.category.splice(i, 1);
                    }
                }
            }

            _target.toggleClass('active');

            if ( !this.data.category.length || this.data.category[0] === '*' && this.data.category.length === 1 ) {
                _target.parent().siblings(':eq(1)').find('span').addClass('active');
                this.data.category = [_target.parent().siblings(':eq(1)').find('span').data('filter')];
            }else {
                _target.parent().siblings(':eq(1)').find('span').removeClass('active');
            }

            if ( this.data.postType === 'post' ) {
                this.changeUrl(this.data);
            }

            this.onAjax('onFilter');
        }

        if ( _target.data('filter-type') !== undefined ) {

            parent.parent().removeClass('open').removeAttr('style');
            parent.parent().find('li:eq(0) span').text(_target.text());

            this.data.postType = _target.data('filter-type');
            this.data.subCat = '';
            this.data.page = 1;
            _target.parent().siblings().find('span')
                .add(this.subFilter.find('span'))
                .removeClass('active');
            _target.parent().find('span').eq(0).addClass('active');

            this.onAjax('onFilter');
        }
    },
    changeUrl(data, event) {
        if ( event === undefined ) {
            window.location.hash = `#post_type=${data.postType}&cat=${data.category}`;
        }else if (window.location.hash !== '') {
            let catArray = window.location.hash.split('cat=')[1].split(','),
                filterBar =  $('.c-filter-bar');

            this.data.category = catArray;
            this.data.page = 1;

            filterBar.find('span').removeClass('active');
            filterBar.find('span').each(function (index, el) {
                for (let i in catArray) {
                    if ( catArray[i] === $(el).data('filter') ) {
                        $(el).addClass('active');
                        if  ( window.innerWidth <= 768 ) {
                            $(el).parents('.c-filter-bar').find('li:eq(0) span').text($(el).text());
                        }
                    }
                }
            }.bind(this));

            this.onAjax('onFilter');
        }
    },
    filterDropDown(target) {
        let _target = $(target),
            height = 0,
            parent = _target.parent();

        if ( _target.data('dropdown') && !parent.is('.open') ) {
            parent.find('li').each(function () {
                if ( $(this).parents('.c-sub-filter-bar').length === 0 ) {

                    height += $(this).innerHeight();
                }
            });

            parent.height(height).addClass('open');

        }else if ( _target.data('dropdown') !== undefined ) {
            parent.removeClass('open').removeAttr('style');
        }
    },
    onScroll(event) {
        if ( event === undefined ) {
            if( this.getScrollPosition() + window.innerHeight >= this.container.outerHeight() + this.container.offset().top + 200) {

                if ( this.data.page <= this.container.data('max-page') && this.ajax ) {

                    this.container.parent().addClass('js-load');
                    this.onAjax('scroll');
                    this.ajax = false;
                }
            }
        }else {
            if ( this.data.page <= this.container.data('max-page') && this.ajax ) {

                this.container.parent().addClass('js-load');
                this.onAjax('scroll');
                this.ajax = false;
            }
        }
    },
    getScrollPosition() {
        return (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
    },
    onAjax(event) {

        console.log( this.data );

        this.loadStatus.addClass('js-show');

        $.post( admin_url.ajax_url, this.data, function(response) {

            if ( response !== 0 ) {
                let responseJson = JSON.parse(response);

                if ( event === 'scroll' ) {
                    this.container.append(responseJson.html);
                    this.data.page++;
                    this.ajax = true;

                    if ( this.data.type === 'team_members' ) {
                        teamMemberInfo.event();
                    }
                }

                if ( event === 'onFilter') {
                    this.container.html(responseJson.html);
                    this.data.page++;
                    this.container.data('max-page', responseJson.maxPage);

                    if ( window.innerWidth >= 768 ) {
                        if (responseJson.htmlSubFilter !== '' && this.subFilter.find('[data-waypoint]').length === 0) {
                            this.subFilter.html(responseJson.htmlSubFilter)
                        }else if (this.data.postType !== 'industry') {
                            this.subFilter.html('');
                        }
                    }
                }

                this.sizeGrid();
                waypoints.reInit($('[data-waypoint]'));

                this.loadStatus.removeClass('js-show');
            }
        }.bind(this));
    },
};

export default postGrid;
