import TweenMax from 'gsap/src/uncompressed/TweenMax.js';
import waypoints from './waypoints';

let tabbed = {
    trigger: $('.c-tabbed__trigger'),
    itemContent: $('.c-tabbed__item'),
    tabbedContent: $('.c-tabbed__content'),
    tabbedList: $('.c-tabbed__list'),
    tabbed: $('.c-tabbed'),
    init() {
        this.event();
        setTimeout(()=>{
            this.activeState(0, 'load');
        }, 700);
    },
    event() {
        let _that = this;

        this.trigger.find('h3').on('click', function () {
            let index = $(this).parent().data('index');

            _that.activeState(index, 'click');
        });

        $(window).on('resize', function () {
            let index;

             _that.trigger.each(function () {
                 if ($(this).is('.active')) index = $(this).data('index');
             });

            _that.activeState(index);
        });
    },
    activeState(index = 0, event) {
        let height = this.itemContent.eq(index).innerHeight(),
            heightOneTrigger = this.tabbedList.find('h3:eq(0)').innerHeight();

        if ( !this.trigger.eq(index).is('.active') ) {
            this.trigger.removeClass('active');
            this.trigger.eq(index).addClass('active');
        }

        this.trigger.add(this.tabbed).removeAttr('style');
        if ( window.innerWidth < 1024 ) {
            let triggerHeight = 0;

            this.trigger.each(function () {
                triggerHeight += this.innerHeight;
            });

            this.trigger.eq(index).css('margin-bottom', height);
            this.tabbed.height(height + triggerHeight);
        }else {
            this.tabbed.height(height + heightOneTrigger);
        }
        this.tabbedContent.height(height);

        if ( event === 'click' ) {
            waypoints.reInit($('[data-waypoint]'));
            if ( window.innerWidth <= 768 ) {
                setTimeout(()=>{
                    TweenMax.to(window, .5, {
                        scrollTo: {
                            y: this.trigger.eq(index).offset().top,
                            autoKill:false,
                        },
                        ease: window.Power4.easeOut,
                        onComplete: function () {
                            console.log( 'scroll' );
                        },
                    });
                }, 500);
            }
        }
    },
};

export default tabbed;