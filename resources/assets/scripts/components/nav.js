import TweenMax from 'gsap/src/uncompressed/TweenMax.js';
require('gsap/src/uncompressed/plugins/ScrollToPlugin.js');

let isOpen = false,
    menuItem = $('.menu-item'),
    menuTrigger = $('[data-trigger="nav"]');

function events(){

    $(document).on('click', function (e) {
        if (!menuTrigger.is(e.target) && menuTrigger.has(e.target).length === 0 && isOpen && !menuItem.is(e.target) && menuItem.has(e.target).length === 0 ) {
            toggle();
        }
    })
    // .on('click', '[data-trigger="nav"]', function () {
    //     toggle();
    // })
    // .on('click', '.menu-item-has-children', function () {
    //     if(window.innerWidth <= 768) {
    //         $(this).toggleClass("active");
    //         $(this).siblings().removeClass("active");
    //     }
    // })
    .on('keyup', function (e) {
        if (e.keyCode === 27 && isOpen) {
            toggle();
        }
    })
    .on('click', '[data-scroll-trigger]', function () {
        TweenMax.to(window, 1.5, {
            scrollTo: {
                y: window.innerHeight,
                autoKill:false,
            },
            ease: window.Power4.easeOut,
        });
    });

    $('[data-trigger="nav"]').on('click', function () {
        toggle();
    });

    $('.menu-item-has-children').on('click', function () {
        if(window.innerWidth <= 768) {
            $(this).toggleClass("active");
            $(this).siblings().removeClass("active");
        }
    });
}
function toggle() {
    isOpen = !isOpen;
    $('body').toggleClass('js-nav-open');
}


export default {
    init() {
        events();
    },
}
