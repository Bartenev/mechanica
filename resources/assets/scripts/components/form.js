import Choices from 'choices.js/assets/scripts/dist/choices.js';
let form = {
    init() {
        this.event();
    },
    event() {
        $(document).bind('gform_post_render', function(){
            this.initChoices();
            this.fileUploadEvent();
        }.bind(this));
    },
    initChoices() {
          $('select').each(function () {
              new Choices(this, {
                  searchEnabled: false,
              });
          });
    },
    fileUploadEvent() {
        let _that = this;

        $('[data-file-upload]').find('.c-button').on('click', function () {
            let inputFile = $(this).siblings('input[type="file"]'),
                inputText = $(this).siblings('input[type="text"]');

            inputFile.trigger('click');
            _that.fileUploadListener(inputFile, inputText);
        });
    },
    fileUploadListener(inputFile, inputText) {
        inputFile.on('change', function () {
            let fullValue = $(this).val().split('\\'),
                value = fullValue[fullValue.length - 1].length > 26 ? fullValue[fullValue.length - 1].slice(0, 26) + '...' : fullValue[fullValue.length - 1];

            inputText.val(value);
        });
    },
};

export default form;