// import mapStyle from "./map-style";
let GoogleMapsLoader = require('google-maps');

let googleMap = {
    mapWrap: document.getElementById('map'),
    map: '',
    init() {
        if ($('#map').length) {
            let _that = this,
                infoPosition;

            GoogleMapsLoader.LIBRARIES = ['geometry', 'places'];
            GoogleMapsLoader.KEY = _that.mapWrap.dataset.googleMapKey;
            GoogleMapsLoader.load(function (google) {
                let latLng = new google.maps.LatLng(_that.mapWrap.dataset.lat, _that.mapWrap.dataset.lng),
                    // googleMapHref = `http://maps.google.com/maps?q=loc:${_that.mapWrap.dataset.lat},${_that.mapWrap.dataset.lng}`,
                    googleMapHref = `https://www.google.com/maps/search/?api=1&query=${_that.mapWrap.dataset.lat},${_that.mapWrap.dataset.lng}&query_place_id=${_that.mapWrap.dataset.placeId}`,
                    myOptions = {
                        draggable: true,
                        scrollwheel: false,
                        zoom: 18,
                        center: latLng,
                        mapTypeControl: false,
                        mapTypeId: google.maps.MapTypeId.HYBRID,
                        zoomControl: true,
                        zoomControlOptions: {
                            position: google.maps.ControlPosition.RIGHT_CENTER,
                        },
                        scaleControl: true,
                        streetViewControl: true,
                        streetViewControlOptions: {
                            position: google.maps.ControlPosition.RIGHT_CENTER,
                        },
                        fullscreenControl: false,
                        clickableIcons: false,
                        // styles: mapStyle,
                    };

                // init map
                _that.map = new google.maps.Map(_that.mapWrap, myOptions);

                // init marker
                let marker = new google.maps.Marker({
                    position: latLng,
                    map: _that.map,
                    icon: new google.maps.MarkerImage(_that.mapWrap.dataset.pin, null, null, null, new google.maps.Size(54,86)),
                    animation: google.maps.Animation.DROP,
                    optimized: false,
                    url: googleMapHref,
                });

                // create infoBox
                let createInfoWindow = `<div class="c-map__info">
                                    <a href="${googleMapHref}" target="_blank">
                                        <img src="${_that.mapWrap.dataset.imageLogo}" alt="Mechanica">
                                    </a>
                                    <a href="${googleMapHref}" target="_blank">
                                        <p>${_that.mapWrap.dataset.content}</p>
                                    </a>
                                 </div>`;

                if ( window.innerWidth > 768 ) {
                    infoPosition = new google.maps.Size(250, 75);
                }else {
                    infoPosition = new google.maps.Size(0, 25);
                }

                // init infoBox
                let infoWindow = new google.maps.InfoWindow({
                    content: createInfoWindow,
                    pixelOffset: infoPosition,
                    position: latLng,
                    maxWidth: 420,
                });

                infoWindow.open(_that.map, marker);

                let interval = setInterval(function () {
                    $('.gm-style-iw').prev().addClass('hide');
                    if ( $('.gm-style-iw').prev().is('.hide') ) {
                        clearInterval(interval);
                    }
                }, 10);


                google.maps.event.addListener(marker, 'click', function () {
                    window.open(this.url);
                });

                google.maps.event.addDomListener(window, 'resize', function() {
                    _that.map.setCenter(latLng);
                });
            });
        }
    },
};

export default googleMap;
