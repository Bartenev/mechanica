require('waypoints/lib/noframework.waypoints.js');

let waypoints = {
    waypointsArray: [],
    init(element = $('[data-waypoint]')) {

        setTimeout(()=>{
            element.each(function(index, item) {
                this.actionHandler(item);
            }.bind(this));
        }, 750);
    },
    reInit(element) {
        window.Waypoint.destroyAll();

        element.each(function(index, item) {
            this.actionHandler(item);
        }.bind(this));
    },
    actionHandler: function (element) {

        let waypoint = new window.Waypoint({
            element: element,
            handler: function (direction) {
                let el = $(this.element);
                if (direction === 'down') {
                    el.addClass('js-in');
                }
                if (direction === 'up') {
                    el.removeClass('js-in');
                }
            },
            offset: '90%',
        });

        this.waypointsArray.push(waypoint);
    },
};

export default waypoints;