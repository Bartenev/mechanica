import YouTubePlayer from 'youtube-player';
import Player from '@vimeo/player';
import loadPage from './load.js';
export let player = '';

let backgroundVideo = {
    provider: $('[data-video-provider]').attr('data-video-provider'),
    source: $('[data-video-id]').attr('data-video-id'),
    init() {
        if (this.provider !== undefined && window.innerWidth > 1024) {
            if (this.provider === 'youtube') {
                this.youtube();
            }
            if (this.provider === 'vimeo') {
                this.vimeo();
            }
        }else {
            loadPage.load();
        }
    },
    youtube() {
        let id = this.source;
        player = new YouTubePlayer(this.provider, {
            loop: true,
            controls: false,
            videoId: id,
            autoplay: true,
            width: window.innerWidth,
            height: window.innerHeight + 100,
            playerVars: {
                playlist: id,
                loop: 1,
            },
        });
        player.mute();
        player.playVideo();
        player.on('ready', function () {
            loadPage.load();
        });
    },
    blur() {

    },
    vimeo() {
        let id = this.source;
        player = new Player(this.provider, {
            id: id,
            background: true,
            height: window.innerHeight + 100,
            autoplay: true,
            playsinline: true,
            muted: true,
            loop: true,
        });
        player.on('loaded', function () {
            loadPage.load();
        });
    },

};
export default backgroundVideo;
