let threeColImgs = {
    init(element = $('[data-three-column-images]')) {
        setTimeout(()=>{
            element.each(function(index, item) {
                this.actionHandler(item);
            }.bind(this));
        }, 750);
    },
    actionHandler: function (element) {
      let imgArray = [],
          perRow = $(element).find('.c-image-card').length,
          newW = $(window).width() / perRow;
      $(element).find('.c-image-card').each(function(i,e) {
        let w = $(e).data('width'), h = $(e).data('height');
        let image = {
          origW: w,
          origH: h,
          ratio: +( Math.round( (h / w)*100 + "e+2") + "e-2" ),
          newH: Math.round(newW * h / w),
        };

        imgArray.push(image);
      });

      let shortest;
      imgArray.forEach(function (img) {
        if (shortest===undefined || shortest > img.ratio) {
          shortest = img.ratio;
        }
      });
      $(element).find('.c-image-card').css('padding-bottom', shortest+'%');
    },
};

export default threeColImgs;
