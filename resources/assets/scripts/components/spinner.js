import swiper from '../components/swiper';
import WheelIndicator from '../libs/wheel-indicator';

export const spinner = {
    init() {
        let container = document.querySelectorAll('.c-spinner');
        if (container.length) {
            this.container = container;
            this.fire();
            this.overflowContainer();
        }
    },
    overflowBodyHelper : $('.js-overflow'),
    overflowContainer() {
        $('body').append('<div class="js-overflow"></div>');
        // setTimeout(function () {
        //     // document.querySelector('.js-overflow').scrollTo(0, 500000);
        // }, 1000);
        this.overflowBodyHelper.css('z-index', -1);
    },
    fire() {
        let slides = $(this.container).find('.c-spinner__item').clone().addClass('c-spinner__item_clone');
        let transitionDelay = 250;
        let scrollDelay = 10;
        let slideLength = null;
        let prevIndex = null;
        $(this.container).find('.c-spinner__inner')
            .prepend(slides)
            .append(slides.clone());
        slideLength = $('.c-spinner__item').length;
        if (window.innerWidth < 768) {
            scrollDelay = 0;
            transitionDelay = 150;
        }

        let sp = new swiper.swiperConstructior(this.container, {
            wrapperClass: 'c-spinner__inner',
            slideClass: 'c-spinner__item',
            direction: 'vertical',
            speed: transitionDelay,
            grabCursor: false,
            touchRatio: 0.15,
            height: 60,
            loop: false,
            on: {
                init: function () {
                    this.slideTo(slides.length);
                    if ( window.innerHeight <= 400 ) {
                        this.params.height = 40;
                        this.update();
                    }
                },
                slideChangeTransitionStart: function () {
                    let index = this.activeIndex;
                    prevIndex = this.previousIndex;
                    let direction = slideDirection(index);

                    if (direction === 'next') {
                        let firstSlide = $('.c-spinner__item').get(0);
                        this.appendSlide(firstSlide.outerHTML);
                        setTimeout(() => {
                            this.removeSlide(0);
                        }, transitionDelay + scrollDelay);
                        prevSlideTransitionEnd();

                    }
                    else if (direction === 'prev') {
                        let lastSlide = $('.c-spinner__item').get(slideLength - 1);
                        setTimeout(() => {
                            this.prependSlide(lastSlide.outerHTML);
                            this.removeSlide(slideLength);
                        }, transitionDelay + scrollDelay);

                        prevSlideTransitionEnd();

                    }
                },
                resize: function () {
                    if ( window.innerHeight <= 400 ) {
                        this.params.height = 40;
                        this.update();
                    }
                },
            },
            navigation: {
                nextEl: '.c-spinner__nav_next',
                prevEl: '.c-spinner__nav_prev',
            },
        });
        swiper.swipersArray.push(sp);


        let indicators = [],
            indicator = new WheelIndicator({
                elem: document.querySelector('.l-hero'),
                scrollSensitivity: 25,
                callback: function (e) {
                    if (e.direction === 'up' ) {
                        sp.slider.slidePrev();
                        this.turnOff();
                        setTimeout(() => {
                            this.turnOn();
                        }, transitionDelay + scrollDelay);
                    }
                    else if (e.direction === 'down' ) {
                        sp.slider.slideNext();
                        this.turnOff();
                        setTimeout(() => {
                            this.turnOn();
                        }, transitionDelay + scrollDelay);
                    }
                },
            });
        indicators.push(indicator);

        function prevSlideTransitionEnd () {
            $('.js-overflow').css('z-index', 1000);
            setTimeout(function () {
                $('.js-overflow').css('z-index', -1);
            }, transitionDelay + scrollDelay);
        }

        function slideDirection(index) {
            let direction = undefined;
            if (index > prevIndex)
                direction = 'next';
            else
                direction = 'prev';
            return direction;
        }
    },
};
