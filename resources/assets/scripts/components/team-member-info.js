import SwipePanel from './swipe-panel';
import postGrid from './post-grid';
import Lazyload from  'vanilla-lazyload/dist/lazyload.js';

let teamMemberInfo = {
    swipePanel: '',
    swipePanelTrigger: $('[data-swipe-panel-trigger]'),
    eventDelegate: $('[data-post-type="team_members"]'),      
    init() {
        if ( this.swipePanelTrigger.length ) {
            this.swipePanel = new SwipePanel(this.swipePanelTrigger[0]);
            console.log( 'inti swipe panel' );
        }
        new Lazyload({
            elements_selector: '.lazyload',
        });
        this.event();
    },
    event() {
        let _that = this;

        this.swipePanelTrigger = $('[data-swipe-panel-trigger]');

        this.eventDelegate.on('click', this.swipePanelTrigger, function (event) {
            let contentMember = event.target.getElementsByClassName('c-team-row__info')[0],
                index = $(event.target).parents('[data-waypoint]').index(),
                dataContent = {
                    'image' : event.target.dataset.image,
                    'title' : contentMember.getElementsByTagName('h2')[0].innerHTML,
                    'content' : $(contentMember.getElementsByClassName('c-team-row__content')[0]).html(),
                    'firstName' : contentMember.dataset.firstName,
                    'email' : contentMember.dataset.email,
                    'phone' : contentMember.dataset.phone,
                    'recentPost' : '',
                    'prevLink' : '',
                    'nextLink' : '',
                };

            if ( contentMember.getElementsByClassName('c-team-row__post').length !== 0 ) {
                dataContent.recentPost = contentMember.getElementsByClassName('c-team-row__post')[0].innerHTML;
            }

            if ( index < $('[data-swipe-panel-trigger]').parent().length - 1 ) {
                let name = $('[data-swipe-panel-trigger]').parent().eq(index + 1).find('.c-team-row__info').data('first-name');
                dataContent.nextLink = ` <a class="c-person-info__button" data-index="${index + 1}" data-next>Meet ${name}</a>`;
            } else {
                dataContent.nextLink = ` <a class="c-person-info__button hide" data-next></a>`;
            }

            if ( index !== 0 ) {
                let name = $('[data-swipe-panel-trigger]').parent().eq(index - 1).find('.c-team-row__info').data('first-name');
                dataContent.prevLink = `<a class="c-person-info__button" data-index="${index - 1}" data-prev>Meet ${name}</a>`;
            }else {
                dataContent.prevLink = `<a class="c-person-info__button hide" data-prev></a>`;
            }

            _that.swipePanel.open(_that.teamContent(dataContent));

        });

        document.addEventListener('click', function (e) {

            if ( $(e.target).is('[data-next]') || $(e.target).is('[data-prev]') ) {
                $(this.swipePanel.container).addClass('js-hide');

                if ( $(e.target).data('index') === $('[data-swipe-panel-trigger]').parent().length - 2 ) {
                    console.log( 'test length' );
                    postGrid.onScroll('trigger');
                }

                setTimeout(()=>{
                    $(this.swipePanel.container).removeClass('js-hide');
                    this.update($(e.target).data('index'))
                }, 200);
            }

        }.bind(this));
    },
    update(index) {
        let nextMember = $('[data-swipe-panel-trigger]').parent().eq(index),
            contentMember = nextMember.find('.c-team-row__info'),
            dataContent = {
                'image' : nextMember.find('[data-image]').data('image'),
                'title' : contentMember.find('h2').html(),
                'content' : contentMember.find('.c-team-row__content').html(),
                'firstName' : contentMember.data('first-name'),
                'email' : contentMember.data('email'),
                'phone' : contentMember.data('phone'),
                'recentPost' : '',
                'prevLink' : '',
                'nextLink' : '',
            };

        if ( contentMember.find('.c-team-row__post').length !== 0 ) {
            dataContent.recentPost = contentMember.find('.c-team-row__post').html();
        }

        if ( index < $('[data-swipe-panel-trigger]').parent().length - 1 ) {
            let name = $('[data-swipe-panel-trigger]').parent().eq(index + 1).find('.c-team-row__info').data('first-name');
            dataContent.nextLink = ` <a class="c-person-info__button" data-index="${index + 1}" data-next>Meet ${name}</a>`;
        } else {
            dataContent.nextLink = ` <a class="c-person-info__button hide" data-next></a>`;
        }

        if ( index !== 0 ) {
            let name = $('[data-swipe-panel-trigger]').parent().eq(index - 1).find('.c-team-row__info').data('first-name');
            dataContent.prevLink = `<a class="c-person-info__button" data-index="${index - 1}" data-prev>Meet ${name}</a>`;
        } else {
            dataContent.prevLink = `<a class="c-person-info__button hide" data-prev></a>`;
        }

        this.swipePanel.open(this.teamContent(dataContent));
    },
    teamContent(content) {
        let connect = `<ul class="c-person-info__recent-posts">
                            <li>connect with <span>${content.firstName}</li>
                        `;

        if ( content.email !== '' ) {
            connect += `<li><a href="mailto:${content.email}">${content.email}</a></li>`;
        }

        if ( content.phone !== '' ) {
            connect += `<li><a href="tel:${content.phone}">${content.phone}</a></li>`;
        }

        connect += `</ul>`;

        if ( content.email === '' && content.phone === '' ) {connect = '';}

        return `<div class="row row_reverse">
                    <div class="col-xs-12 col-sm-6">
                        <div class="c-person-photo" style="background-image: url(${content.image})">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 p-x-20 p-x-sm-40">
                        <div class="c-person-info">
                            <div class="c-person-info__content">
                                <h3 class="c-person-info__name">${content.title}</h3>
                                <div class="c-person-info__post" data-editor>${content.content}</div>
                                <ul class="c-person-info__recent-posts">${content.recentPost}</ul>
                                ${connect}
                            </div>
                            <div class="c-person-info__link">
                                ${content.prevLink}
                                ${content.nextLink}
                            </div>
                        </div>
                    </div>
                </div>`;
    },
};

export default teamMemberInfo;
