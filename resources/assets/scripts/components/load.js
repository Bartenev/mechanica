import TweenMax from 'gsap/src/uncompressed/TweenMax.js';
import waypoints from "./waypoints";
import backgroundVideo from "./background-video";

let loadPage = {
    verticalBarBG: $('.c-vertical-bar__bg'),
    hamburgerLine : $('.c-hamburger .c-hamburger__bar_animate'),
    logo: $('.c-header-info__logo'),
    tagLine: $('.c-header-info__tagline'),
    init() {
        if ( $('body').is('.home') ) {
            backgroundVideo.init();
        }else {
            this.load();
        }
    },
    load() {
        let _that = this;

        setTimeout(()=>{
            // animate vertical red line
            TweenMax.to(_that.verticalBarBG, 1,
                {
                    y: 0,
                    ease: window.Power4.easeOut,
                }
            );
            $('.c-vertical-bar').attr('data-loaded',true);

            waypoints.init();

            setTimeout(()=>{
                // animate hamburger white lines
                _that.hamburgerLine.each(function (index, el) {
                    TweenMax.fromTo(el, .5,
                        {
                            y: 5,
                        },
                        {
                            y: 0,
                            alpha: 1,
                            ease: window.Power0.easeOut,
                            delay: 0.1 * index,
                        }
                    );
                });
                // animate logo
                TweenMax.to(_that.logo, .5, {
                    y: 0,
                    alpha: 1,
                    ease: window.Power0.easeOut,
                    delay: .4,
                });
                // animate tag line
                if ( _that.tagLine.length ) {
                    TweenMax.to(_that.tagLine, .5, {
                        y: 0,
                        alpha: 1,
                        ease: window.Power0.easeOut,
                        delay: .5,
                    });
                }
                _that.loadHome();
            }, 300);
        }, 1000);
    },
    loadHome() {
        if ( $('body').is('.home') ) {
            setTimeout(()=>{
                let introText = $('.c-intro-text p'),
                    text = $('.c-title p'),
                    spinner = $('.c-spinner');

                // animate Home intro text
                introText.each(function (index, el) {
                    let alpha = index === 0 ? .48 : 1;

                    TweenMax.to(el, .3,
                        {
                            y: 0,
                            alpha: alpha,
                            ease: window.Power0.easeInOut,
                            delay: .25 * index,
                            onComplete() {
                                setTimeout(()=>{
                                    TweenMax.fromTo(el, .3,
                                        {
                                            y: 0,
                                            alpha: alpha,
                                        },
                                        {
                                            y: 25,
                                            alpha: 0,
                                            ease: window.Power0.easeInOut,
                                            delay: .50 * (introText.length - index),
                                            onComplete() {
                                                if ( index === 0 ) {
                                                    backgroundVideo.blur();
                                                }
                                                if ( index === introText.length - 1) {
                                                    setTimeout(()=>{
                                                        if ( text.length ) {
                                                            text.each(function (index, el) {
                                                                TweenMax.to(el, .3, {
                                                                    y: 0,
                                                                    alpha: 1,
                                                                    ease: window.Power0.easeInOut,
                                                                    delay: .25 * index,
                                                                    onComplete() {
                                                                        if (index === 0) {
                                                                            TweenMax.fromTo(spinner, .3,
                                                                                {
                                                                                    y: 25,
                                                                                    alpha: 0,
                                                                                },
                                                                                {
                                                                                    y: 0,
                                                                                    alpha: 1,
                                                                                    delay: .25,
                                                                                    ease: window.Power0.easeInOut,
                                                                                }
                                                                            );
                                                                        }
                                                                    },
                                                                });
                                                            });
                                                        } else {
                                                            TweenMax.fromTo(spinner, .8, {
                                                                y: 0,
                                                                alpha: 1,
                                                                ease: window.Power0.easeOut,
                                                            });
                                                        }
                                                    }, 1000);
                                                }
                                            },
                                        }
                                    );
                                }, 300);
                            },
                        }
                    );
                });
            }, 1000);
        }
    },
};

export default loadPage;
