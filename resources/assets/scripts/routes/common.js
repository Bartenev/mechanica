import navigation from '../components/nav';
import lightbox from '../components/lightbox';
import swiper from '../components/swiper';
import postGrid from '../components/post-grid';
import {spinner} from '../components/spinner';
import instagram from '../components/instagram';
import teamMemberInfo from '../components/team-member-info';
import googleMap from '../components/google-map';
// import backgroundVideo from "../components/background-video";
import tabbed from "../components/tabbed";
import form from "../components/form";
import threeColImgs from "../components/three-column-images";
// import waypoints from "../components/waypoints";
import loadPage from "../components/load";

export default {
    init() {
        // JavaScript to be fired on all pages
        loadPage.init();
        // waypoints.init();
        navigation.init();
        swiper.init();
        postGrid.init();
        spinner.init();
        instagram.init();
        teamMemberInfo.init();
        googleMap.init();
        tabbed.init();
        form.init();
        threeColImgs.init();
    },
    finalize() {
        lightbox.init();
        // JavaScript to be fired on all pages, after page specific JS is fired
    },

};
