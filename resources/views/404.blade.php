@extends('layouts.app')

@section('content')
    @php
        $args = array(
            'page_id' => 553,
            'posts_per_page' => 1
        ) ;

        $query = new WP_Query( $args );
    @endphp

    @if ($query->have_posts())
        @while ( $query->have_posts() )
            @php $query->the_post() @endphp
            <div class="alert alert-warning">
                @include('partials.content-page')
            </div>
        @endwhile
    @endif
@endsection