{{--
  Template Name: Blog Page
--}}

@extends('layouts.app')

@section('content')
    @if ($featured_blog_post)
        <div class="l-viewport">
            <div class="c-featured-blog-post" data-waypoint>
                <a href="{{$featured_blog_post['url']}}" class="c-featured-blog-post__image" data-fade-in style="background-image: url({{$featured_blog_post['image']}});"></a>
                <div class="c-featured-blog-post__footer" data-slide-up>
                    <span class="c-button c-button_scroll" data-scroll-trigger data-duration-03s></span>
                    <div class="c-feature-blog-post-info"  >
                        <a href="{{$featured_blog_post['url']}}" class="c-feature-blog-post-info__item">
                            <h1 class="c-feature-blog-post-info__title">
                                <span>{{$featured_blog_post['category']}}</span>
                                {{html_entity_decode($featured_blog_post['title'])}}
                            </h1>
                            <p>

                                @if ( strlen($featured_blog_post['excerpt']) > 97 )
                                    {!! substr($featured_blog_post['excerpt'], 0, 94) . '...' !!}
                                @else
                                    {!! $featured_blog_post['excerpt'] !!};
                                @endif
                            </p>
                        </a>
                        <div class="c-feature-blog-post-info__item" data-duration-02s>
                            @if($featured_blog_post['category']!=='Press')<span class="c-feature-blog-post-info__author">By {{$featured_blog_post['author']}}</span>
                            <a href="{{$featured_blog_post['url']}}" class="c-button">READ MORE</a>@endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if($news_category)
        <div class="l-container l-container_fluid l-container_fluid_content" data-waypoint>
            <ul class="c-filter-bar" data-slide-up>
                <li data-dropdown="true">Filter by: <span>All</span></li>
                <li data-duration-0s><span data-filter="*" class="active">All</span></li>
                @foreach($news_category as $key => $cat)
                    @php
                        $cat_post = get_posts(array( 'category_name'=>$cat['cat_slug'], 'numberposts' => -1, 'post_status' => 'publish', 'exclude' => $featured_blog_post['id'] ));
                        $count_post = count($cat_post);
                    @endphp
                    @if ($count_post > 0)
                        <li data-duration-0<?php echo $key + 1; ?>s><span data-filter="{{$cat['cat_slug']}}">{{$cat['cat_name']}}</span></li>
                    @endif
                @endforeach
            </ul>
        </div>
    @endif
    @if($news_posts)
        <div class="l-wrap">
            <div class="row row_gutter"
                 data-container-ajax
                 data-post-type="post"
                 data-max-page="{{$news_posts['max_num_pages']}}"
                 data-exclude-post="{{$featured_blog_post['id']}}">
                @foreach($news_posts['query_date'] as $key => $news)
                    @php
                        $author_id = get_post_field ('post_author', $news['id']);
                        $author_name = get_the_author_meta('first_name', $author_id) . ' ' . get_the_author_meta('last_name', $author_id);
                    @endphp
                    @if ( count($news_posts['query_date']) >= 7 && $news['count'] == 6 || $news['count'] == 7 )

                        @if ( $news['count'] == 6 ) <div class="col-xs-12 col-sm-4" data-waypoint> @endif
                            <a href="{{$news['url']}}" class="row" data-slide-up>
                                <div class="col-xs-12" data-duration-02s>
                                    <div class="c-featured-post c-featured-post_medium @php echo $news['count'] == 6 ? 'm-b-12' : '' @endphp">
                                        <div class="c-featured-post__background <?php echo strtolower($news['category']); ?>" style="background-image: url({{$news['image']}});"></div>
                                        <div class="c-featured-post__content">
                                            <h2>
                                                <span>{{$news['category']}}</span>
                                                {{html_entity_decode($news['title'])}}
                                            </h2>
                                            @if($news['category']!=='Press')<span>By {{$author_name}}</span>@endif
                                        </div>
                                    </div>
                                </div>
                            </a>
                        @if ( $news['count'] == 7 ) </div> @endif

                    @else
                        @php
                            $animation_duration = 'data-duration-0s';
                            if ($key == 1 || $key == 4 ) $animation_duration = 'data-duration-01s';
                        @endphp
                        <a href="{{$news['url']}}" class="col-xs-12 col-sm-{{$news['size']}}" data-waypoint>
                            <div class="c-featured-post" data-slide-up>
                                <div class="c-featured-post__background <?php echo strtolower($news['category']); ?>" {{$animation_duration}} style="background-image: url({{$news['image']}});"></div>
                                <div class="c-featured-post__content" {{$animation_duration}}>
                                    <h2>
                                        <span>{{$news['category']}}</span>
                                        {{$news['title']}}
                                    </h2>
                                    @if($news['category']!=='Press')<span>By {{$author_name}}</span>@endif
                                </div>
                            </div>
                        </a>
                    @endif
                @endforeach
            </div>
            <div class="с-loader-ellipse">
                <span class="с-loader-ellipse__dot"></span>
                <span class="с-loader-ellipse__dot"></span>
                <span class="с-loader-ellipse__dot"></span>
                <span class="с-loader-ellipse__dot"></span>
            </div>
        </div>
    @endif
@endsection
