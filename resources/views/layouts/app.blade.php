<!doctype html>
<html @php language_attributes(); @endphp>
@include('partials.head')
<body @php body_class(); @endphp>
    @php
      if ($_SERVER['PANTHEON_ENVIRONMENT']==='live') {
        echo get_field('body_top', 'option');
      }
      do_action('get_header');
    @endphp
    @include('partials.header')
    <div class="l-page-wrap" role="document">
        <main class="l-page-main">
            @yield('content')
        </main>
        @if (App\display_sidebar())
            <aside class="l-page-sidebar">
                @include('partials.sidebar')
            </aside>
        @endif
    </div>
    @php do_action('get_footer'); @endphp
    @include('partials.footer')
    @php
      wp_footer();
      if ($_SERVER['PANTHEON_ENVIRONMENT']==='live') {
        echo get_field('body_bottom', 'option');
      }
    @endphp
</body>
</html>
