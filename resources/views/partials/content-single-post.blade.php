@php
    $featured_image = App::getImageSrc(get_post_thumbnail_id());
    $post_title = html_entity_decode(get_the_title());
    $post_date = get_the_date();
    $post_list_url = get_permalink(22);
    $post_category = get_the_category();
@endphp
<div class="c-post-header" data-waypoint>
    <div class="c-post-header__inner" style="background-image: url({{$featured_image}})" data-fade-in>
        <div class="c-post-header__content" data-slide-up>
            <div class="c-post-header__info" data-duration-0s>
                <div class="c-header-content__breadcrumbs">
                    <a class="c-header-content__breadcrumb" href="{{$post_list_url}}">Back
                        to Voices</a>
                </div>
                {{--<p class="c-post-header__category">--}}
                    {{--@php--}}
                        {{--$i = 0;--}}
                        {{--$len = count($post_category);--}}
                    {{--@endphp--}}
                    {{--@foreach($post_category as $category)--}}
                        {{--{{$category -> name}}--}}
                        {{--@php--}}
                            {{--if ($len > 1 && !$i == $len - 1) {--}}
                                {{--echo ', ';--}}
                            {{--}--}}
                            {{--$i++;--}}
                        {{--@endphp--}}
                    {{--@endforeach--}}
                {{--</p>--}}
                <h2 class="c-post-header__title @if(strlen($post_title) > 40)shrink @endif">{{$post_title}}</h2>
            </div>
            <p class="c-post-header__date" data-duration-01s>{{$post_date}}</p>
        </div>
    </div>
</div>
<div class="col p-x-20 p-t-40 p-b-80 p-x-sm-60 p-t-sm-80 p-b-sm-110">
    <div data-waypoint>
        <div class="c-post-content" data-editor data-slide-up>
            {{the_content()}}
        </div>
    </div>
</div>
