<div class="c-vertical-bar">
    @if ( is_front_page() )
        <div class="c-vertical-bar__logo">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 54 47.97" width="135" height="120">
                <defs>
                    <style>.logo-line{fill:#da281c;}</style>
                </defs>
                <title>Asset 2</title>
                <g id="logo-line" data-name="logo-line">
                    <g id="logo-line-1" data-name="logo-line-1">
                        <polygon class="logo-line" points="37.28 0 27 16.86 16.72 0 0 0 0 47.97 15.69 47.97 15.69 24.19 26.73 41.25 27 41.25 38.03 24.19 38.03 47.97 54 47.97 54 0 37.28 0"/>
                    </g>
                </g>
            </svg>
        </div>
    @endif
    <div class="c-vertical-bar__bg"></div>
</div>
<div class="l-menu-background__main"></div>
<div class="l-menu-background__overlay"></div>
<header class="l-header">
    <div class="c-hamburger" data-trigger="nav">
        <div class="c-hamburger__bar c-hamburger__bar_animate"></div>
        <div class="c-hamburger__bar c-hamburger__bar_animate"></div>
        <div class="c-hamburger__bar c-hamburger__bar_animate"></div>
        <div class="c-hamburger__bar">
            {{--<div class="c-hamburger__bar-panel"></div>--}}
        </div>
        <div class="c-hamburger__bar">
            {{--<div class="c-hamburger__bar-panel"></div>--}}
        </div>
    </div>
    @php
        $logo_color = get_field('logo_color');
        $tagline = get_field('tagline', 'option');
    @endphp
    <div class="c-header-info c-header-info__{{$logo_color}}">
        <a class="c-header-info__logo" href="{{ home_url('/') }}">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 267.1 29.127">
                <defs>
                    <style>
                        .logo {
                            fill: #fff;
                        }
                    </style>
                </defs>
                <title>logo</title>
                <path class="logo" d="M0,.5H8.2l6.7,10.8L21.6.5h8.2v28H22.1V12.4l-7.2,11h-.2L7.6,12.5v16H0ZM36.1.5H58.6V7.1H43.8v4.2H57.2v6.2H43.8v4.4h15v6.6H36.1ZM63.6,14.6h0A14.41,14.41,0,0,1,77.813,0c.2,0,.391,0,.587,0A13.869,13.869,0,0,1,90.3,5.8l-5.8,4.5C82.9,8.3,81.1,7,78.4,7c-3.9,0-6.7,3.3-6.7,7.4v.1c0,4.2,2.8,7.5,6.7,7.5,2.9,0,4.6-1.4,6.3-3.4l5.8,4.2a14.149,14.149,0,0,1-12.4,6.3A14.26,14.26,0,0,1,63.6,15.084C63.6,14.923,63.6,14.761,63.6,14.6ZM95.5.5h7.8V11h10V.5h7.8v28h-7.9V17.9h-10V28.5H95.4V.5ZM137.1.3h7.5l11.9,28.2h-8.3l-2-5H135.4l-2,5h-8.2Zm6.8,17.2-3.1-8-3.2,8ZM160.7.5h7.2l11.5,14.8V.5h7.7v28h-6.8l-12-15.4V28.5h-7.7V.5Zm32.5,0H201v28h-7.8Zm13.2,14.1h0A14.41,14.41,0,0,1,220.613,0c.2,0,.392,0,.587,0a13.869,13.869,0,0,1,11.9,5.8l-5.8,4.5c-1.6-2-3.4-3.3-6.1-3.3-3.9,0-6.7,3.3-6.7,7.4v.1c0,4.2,2.8,7.5,6.7,7.5,2.9,0,4.6-1.4,6.3-3.4l5.8,4.2a14.79,14.79,0,0,1-26.9-8.2ZM247.7.3h7.5l11.9,28.2h-8.3l-2-5H246l-2,5h-8.2Zm6.8,17.2-3.1-8-3.2,8Z" transform="translate(0 0.013)"/>
            </svg>

        @if ( !empty($tagline) )
                <p class="c-header-info__tagline">{{$tagline}}</p>
            @endif
        </a>
    </div>
    <div class="c-header-info c-header-info_fixed">
        <a class="c-header-info__logo c-header-info__white" href="{{ home_url('/') }}">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 267.1 29.127">
                <defs>
                    <style>
                        .logo {
                            fill: #fff;
                        }
                    </style>
                </defs>
                <title>logo</title>
                <path class="logo" d="M0,.5H8.2l6.7,10.8L21.6.5h8.2v28H22.1V12.4l-7.2,11h-.2L7.6,12.5v16H0ZM36.1.5H58.6V7.1H43.8v4.2H57.2v6.2H43.8v4.4h15v6.6H36.1ZM63.6,14.6h0A14.41,14.41,0,0,1,77.813,0c.2,0,.391,0,.587,0A13.869,13.869,0,0,1,90.3,5.8l-5.8,4.5C82.9,8.3,81.1,7,78.4,7c-3.9,0-6.7,3.3-6.7,7.4v.1c0,4.2,2.8,7.5,6.7,7.5,2.9,0,4.6-1.4,6.3-3.4l5.8,4.2a14.149,14.149,0,0,1-12.4,6.3A14.26,14.26,0,0,1,63.6,15.084C63.6,14.923,63.6,14.761,63.6,14.6ZM95.5.5h7.8V11h10V.5h7.8v28h-7.9V17.9h-10V28.5H95.4V.5ZM137.1.3h7.5l11.9,28.2h-8.3l-2-5H135.4l-2,5h-8.2Zm6.8,17.2-3.1-8-3.2,8ZM160.7.5h7.2l11.5,14.8V.5h7.7v28h-6.8l-12-15.4V28.5h-7.7V.5Zm32.5,0H201v28h-7.8Zm13.2,14.1h0A14.41,14.41,0,0,1,220.613,0c.2,0,.392,0,.587,0a13.869,13.869,0,0,1,11.9,5.8l-5.8,4.5c-1.6-2-3.4-3.3-6.1-3.3-3.9,0-6.7,3.3-6.7,7.4v.1c0,4.2,2.8,7.5,6.7,7.5,2.9,0,4.6-1.4,6.3-3.4l5.8,4.2a14.79,14.79,0,0,1-26.9-8.2ZM247.7.3h7.5l11.9,28.2h-8.3l-2-5H246l-2,5h-8.2Zm6.8,17.2-3.1-8-3.2,8Z" transform="translate(0 0.013)"/>
            </svg>
            @if ( !empty($tagline) )
                <p class="c-header-info__tagline">{{$tagline}}</p>
            @endif
        </a>
    </div>
    <nav class="c-nav-menu">
        @if (has_nav_menu('primary_navigation'))
            {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
        @endif
    </nav>

</header>
