<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,900" rel="stylesheet">
    <link type="text/plain" rel="author" href="<?php echo get_template_directory_uri(); ?>/humans.txt" />
    @php
      wp_head();
      if ($_SERVER['PANTHEON_ENVIRONMENT']==='live') {
        echo get_field('head', 'option');
      }
    @endphp
    <meta name="theme-color" content="#da281c">
</head>
