@php
    $background = get_field('background_type') == 'image' ? 'background-image: url(' . App::getImageSrc(get_field('background_image')) . ')' : 'background-color:' . get_field('background_color');
    $breadcrumb_link = get_field('breadcrumb_link');
    $title = !empty(get_field('title')) ? get_field('title') : App::title();
    $description = get_field('description');
    $content_theme = get_field('content_theme');
    $breadcrumb_link = get_field('breadcrumb_link');
    $sab_title = get_field('sab_title');
    $inquire_button = array(
        'to' => get_field('inquire_button_to'),
        'subject' => get_field('inquire_button_subject'),
        'body' => get_field('inquire_button_body')
    );    

    $responsibilities = get_field('responsibilities');
    $qualifications = get_field('qualifications');
    $description = get_field('description');
    $callout = acf_is_empty(get_field('callout')) ? 'We look forward to hearing from you!' : get_field('callout');    
@endphp

<div class="l-page-header" data-waypoint>
    <div class="l-page-header__inner" style="{{$background}}" data-fade-in>
        <div class="l-page-header__section" data-{{$content_theme}} data-slide-up>
            <div class="c-header-content" data-duration-01s>
                @php
                    $header_breadcrumb_title = get_field('header_breadcrumb_title', 'option');
                    $header_breadcrumb_link = get_field('header_breadcrumb_link', 'option');
                @endphp
                <div class="c-header-content__breadcrumbs">
                    <a class="c-header-content__breadcrumb" href="{{$header_breadcrumb_link}}">Back
                        to {{$header_breadcrumb_title}}</a>
                </div>
                <h1 class="c-header-content__title">{{ $title }}</h1>
            </div>
        </div>
        <a href="mailto:{{$inquire_button['to']}}?subject={{$inquire_button['subject']}}&body={{$inquire_button['body']}}"
           class="l-page-header__section"
           data-{{$content_theme}}
           data-slide-up>
            <div class="c-button c-button_large" data-duration-02s><span>INQUIRE</span></div>
        </a>
    </div>
</div>
<div class="row" data-waypoint>
    <div class="p-x-20 p-t-40 p-t-sm-50 p-b-30 p-x-sm-100" style="background-color: #e3df88" data-fade-in>
        <div data-slide-up>
            {!! $description !!}
        </div>
    </div>
</div>
<div data-waypoint>
    <div class="row p-t-50 p-b-50 p-x-20 p-x-sm-60" data-slide-up>
        @php $count = 0; @endphp
        @while (have_rows('columns'))
            @php
                the_row();
                $text_content = get_sub_field('text_content');
            @endphp
            <div class="col-xs-12 col-sm-6 u-max-width" data-duration-<?php echo $count == 0 ? $count : '0' . $count;  ?>s>
                <div class="c-career-list" data-editor>
                    {!! $text_content !!}
                </div>
            </div>
            @php $count++; @endphp
        @endwhile
    </div>
</div>
<div data-waypoint>
    <div class="c-text-callout-with-button" data-slide-up>
        <p class="c-text-callout-with-button__title">{{$callout}}</p>
        @if (!empty($inquire_button))
            <a href="mailto:{{$inquire_button['to']}}?subject={{$inquire_button['subject']}}&body={{$inquire_button['body']}}" class="c-button c-button_large">                
                INQUIRE
            </a>
        @endif
    </div>
</div>
@include('partials.acf-rows')