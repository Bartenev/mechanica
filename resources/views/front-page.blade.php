@extends('layouts.app')

@section('content')
    @php
        $intro_text = get_field('intro_text');
        $spinner_title = get_field('title');
        $background_type = get_field('background_type');
        $image = App::getImageSrc(get_field('image'));
        $video_provider = get_field('video_provider');
        $video_src = get_field('video_src');
        $video_poster = App::getImageSrc(get_field('video_poster'));
    @endphp
    <div class="l-hero" data-waypoint>
        <div class="c-blur-media" data-fade-in>
            @if ($background_type === 'video')
                <div class="c-poster" style="background-image:url({{$video_poster}});"></div>
                <div class="iframe-wrapper" id="iframe-wrap"
                     data-video-provider="{{$video_provider}}"
                     data-video-id="{{$video_src}}">
                    <div id="{{$video_provider}}"></div>
                </div>

                <div class="c-blur-media__mask"></div>
            @elseif ($background_type === 'image')
                {{--Image--}}
                <div class="c-poster c-poster_bg" style="background-image:url({{$image}});"></div>
            @endif
        </div>

        <div class="l-hero__screen l-hero__screen_right-side">
            <div class="c-intro-text">
                @while (have_rows('intro_text'))
                    @php
                        the_row();
                        $text = get_sub_field('text');
                    @endphp
                    <p>{!! $text !!}</p>
                @endwhile
            </div>
            <div class="l-hero__screen l-hero__screen_vertical-center">
                <h1 class="c-title c-title_spinner">{!!$spinner_title!!}</h1>
                <div class="u-overflow-hidden">
                    <div class="c-spinner">
                        <div class="c-spinner__nav c-spinner__nav_prev"><i class="arrow"></i></div>
                        <div class="c-spinner__inner">
                            @while (have_rows('spinner_links'))
                                @php
                                    the_row();
                                    $link = get_sub_field('link');
                                @endphp
                                <div class="c-spinner__item">
                                    <a href="{!! $link['url'] !!}" target="{{$link['target']}}">{!! $link['text'] !!}</a>
                                </div>
                            @endwhile
                        </div>
                        <div class="c-spinner__nav c-spinner__nav_next"><i class="arrow"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
