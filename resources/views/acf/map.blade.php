<?php
    $location_selection = get_sub_field('location_selection');
    $google_map_key = 'AIzaSyANZTHgoePZNWB3Yt3g4-XYDccHONEJZvw';
    $logo_image = wp_get_attachment_image_src(get_sub_field('logo_image'), 'thumbnail')[0];
    $text_content = get_sub_field('text_content');
    $place = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location='. $location_selection['lat'] . ',' . $location_selection['lng'] . '&radius=500&keyword=mechanica&key='. $google_map_key;
    $place_json = @file_get_contents( $place );
    $place_attr = json_decode( $place_json, true );
    $place_id = $place_attr['results'][0]['place_id'];
?>
<div class="l-viewport" data-waypoint>
    <div class="c-map-wrap" data-slide-up>
        <div id="map" class="c-map"
             data-lat="{{$location_selection['lat']}}"
             data-lng="{{$location_selection['lng']}}"
             data-google-map-key="{{$google_map_key}}"
             data-pin="@asset('images/map-pin.png')"
             data-image-logo="{{$logo_image}}"
             data-content="{{$text_content}}"
             data-place-id="{{$place_id}}"
             data-duration-0s>
        </div>
        <div class="c-map-wrap__footer" data-duration-01s>
            <span class="c-button c-button_scroll" data-scroll-trigger data-duration-02s></span>
        </div>
    </div>
</div>