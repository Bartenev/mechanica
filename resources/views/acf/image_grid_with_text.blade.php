@php
    $count_cols = count(get_sub_field('columns'));
    $count_item = 0;
@endphp

<div class="l-wrap" data-waypoint>
    <div class="row row_gutter" data-slide-up>
        @while (have_rows('columns'))
            @php
                the_row();
                $count_rows = count(get_sub_field('rows'));
            @endphp
            <div class="col-xs-12 col-sm-{{12 / $count_cols}}" data-duration-<?php echo $count_item == 0 ? 0 : '0' . $count_item; ?>s>
                @while (have_rows('rows'))
                    @php
                        the_row();
                        $type = get_sub_field('content_type');
                        $text = get_sub_field('text');
                        $image = App::getImageSrc(get_sub_field('image'));
                    @endphp
                    @if ($count_rows > 1)
                        <div class="row {{ $count_rows == 1 ? 'row_gutter' : '' }}">
                            <div class="col-xs-12">

                                @if ($type == 'text')
                                    <div class="c-text-card">
                                        <div class="c-text-card__content">
                                            <div class="c-card-editor">
                                                {!! $text !!}
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="c-image-card" style="background-image: url({{$image}});" ></div>
                                @endif

                            </div>
                        </div>
                    @else
                        @if ($type == 'text')
                            <div class="c-text-card">
                                <div class="c-text-card__content">
                                    <div class="c-card-editor">
                                        {!! $text !!}
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="c-image-card" style="background-image: url({{$image}});" ></div>
                        @endif
                    @endif
                @endwhile
            </div>
            @php $count_item++; @endphp
        @endwhile
    </div>
</div>
