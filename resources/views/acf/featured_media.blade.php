<div class="l-wrap">
    <div class="row row_gutter" data-waypoint>
        @php
            $count = count(get_sub_field("content"));
            $count_item = 0;
        @endphp

        @if ( have_rows('content') )
            @while ( have_rows('content'))
                @php
                    the_row();
                    $has_video = get_sub_field('use_video');
                    $video_source = get_sub_field('video_url');
                    $description_text = get_sub_field('description_text');
                    $featured_image = App::getImageSrc(get_sub_field('featured_image'));
                @endphp

                <div class="col-xs-12 col-sm-{{ 12 / $count }}" data-slide-up>
                    <div class="c-featured-media" data-type="{{ $has_video ? 'video' : 'image' }}" data-duration-<?php echo $count_item == 0 ? 0 : '0' . $count_item; ?>s>

                        <div class="c-featured-media__background" style="background-image: url({{$featured_image}});"></div>

                        @if( $has_video || $description_text )
                            
                            <div class="c-featured-media__overlay">
                                <div class="c-featured-media__content">

                                    <div class="c-card-editor c-card-editor_white">
                                        {!! $description_text !!}
                                    </div>

                                    @if ($has_video)
                                        <div class="c-featured-media__cta">
                                            <button class="c-btn-play" data-lightbox-trigger data-lightbox-media="{{$video_source}}" ></button>
                                        </div>
                                    @endif

                                </div>
                            </div>
                        @endif

                    </div>
                </div>
                @php $count_item++; @endphp
            @endwhile
        @endif
    </div>
</div>
