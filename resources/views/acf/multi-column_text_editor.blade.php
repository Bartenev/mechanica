@php
    $count = count(get_sub_field("columns"));
    $padding = '';
    switch (true) {
        case ( $count == 2 ) :
            $size = 6;
            $padding = 'p-x-sm-40';
            break;
        case ( $count == 3 ) :
            $size = 4;
            $padding = 'p-x-sm-40';
            break;
        case ( $count == 4 ) :
            $size = 3;
            $padding = 'p-x-sm-20';
            break;

    }
    $count_item = 0;
@endphp
@if ( have_rows('columns') )
    <div class="l-container l-container_large-width p-t-60 p-t-lg-120 p-b-60 p-b-lg-120 u-content-box" data-content>
        <div class="row @php echo $count == 1 ? 'row_center' : 'row_between' @endphp" data-waypoint>
            @while (have_rows('columns'))
                @php
                    the_row();
                @endphp
                <div class="col-xs-12 col-sm-{{$size}} p-b-20 p-b-sm-0 {{$padding}}" data-slide-up>
                    <div class="c-text-editor" data-editor data-duration-<?php echo $count_item == 0 ? 0 : '0' . $count_item; ?>s>
                        {!!  get_sub_field('text_editor') !!}
                    </div>
                </div>
                @php $count_item++; @endphp
            @endwhile
        </div>
    </div>
@endif
