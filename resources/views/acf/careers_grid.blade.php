@php
    $careers_category = get_terms('careers_category');
@endphp
@if (!empty($careers_category))
    <div class="l-container l-container_fluid l-container_fluid_content" data-waypoint>
        <ul class="c-filter-bar" data-slide-up>
            <li data-dropdown="true">Filter by: <span>All</span></li>
            <li data-duration-0s><span data-filter="*" class="active">All</span></li>
            @foreach($careers_category as $key => $cat)
                <li data-duration-0<?php echo $key + 1; ?>s><span data-filter="{{$cat->slug}}">{{$cat->name}}</span></li>
            @endforeach
        </ul>
    </div>
@endif
@php
    $args = array(
        'post_type' => 'careers',
        'post_date' => 'DESC',
        'posts_per_page' => 12
    );

    $query = new WP_Query( $args );
    $count = 1;
    $row = 1;
    $count_animation = 0;
@endphp
@if ( $query->have_posts() )
    <div class="l-wrap">
        <div class="c-career-post-wrap">
            <div class="row"
                 data-container-ajax
                 data-post-type="careers"
                 data-max-page="{{$query->max_num_pages}}">
                @while ( $query->have_posts() )
                    @php
                        $query->the_post();
                        $link = get_the_permalink();
                        $image = App::getImageSrc(get_post_thumbnail_id(), 'medium_large');
                        $title = get_the_title();

                        if ( !empty( get_post_thumbnail_id()) ) {
                            $image = '<div class="c-career-post__background" style="background-image: url(' . $image . ')"></div>';
                            return;
                        }else {
                            $image = '<div class="c-career-post__background"></div>';
                        }
                    @endphp
                    <a href="{{$link}}" class="col-xs-12 col-sm-4 col-lg-3" data-waypoint>
                        <div data-slide-up>
                            <div class="c-career-post" data-duration-@php echo $count_animation == 0 ? 0 : '0' . $count_animation . 's'; @endphp>
                                @php echo $image @endphp
                                <h2>{{$title}}</h2>
                            </div>
                        </div>
                    </a>
                @php
                    if ( $count == 4 ) {$row++; $count = 0;}
                    $count++;
                    $count_animation == 3 ? $count_animation = 0 : $count_animation++;
                @endphp
                @endwhile
            </div>
            <div class="с-loader-ellipse">
                <span class="с-loader-ellipse__dot"></span>
                <span class="с-loader-ellipse__dot"></span>
                <span class="с-loader-ellipse__dot"></span>
                <span class="с-loader-ellipse__dot"></span>
            </div>
        </div>
    </div>
@endif
@php wp_reset_query(); @endphp