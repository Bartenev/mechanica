@php
    $heading_left = get_sub_field('heading_left');
    $public_email_address = get_sub_field('public_email_address');
    $email_address = get_field('email_address', 'option');
    $phone_number = get_sub_field('phone_number');
    $phone_number_v = get_field('phone_number_v', 'option');
    $phone_number_f = get_field('phone_number_f', 'option');
    $social_media_accounts = get_sub_field('social_media_accounts');
    $heading_right = get_sub_field('heading_right');
    $text_content = get_sub_field('text_content');
    $form = get_sub_field('form');
@endphp
<div class="row" data-waypoint style="position: relative; z-index: 1">
    <div class="col-xs-12 col-md-7 p-t-40 p-x-20 p-x-md-60" data-slide-up>
        <div class="c-contact-info" data-duration-0s>
            <h2 class="c-contact-info__heading">{{$heading_left}}</h2>
            @if ($public_email_address)
                <p class="c-contact-info__email"><a href="mailto:{{$email_address}}">{{$email_address}}</a></p>
            @endif
            @if ($phone_number)
                <p class="c-contact-info__phone">
                    <a href="tel:{{$phone_number_v}}"><span>V</span>{{$phone_number_v}}</a>
                    <span class="c-contact-info__phone__transference"> / </span>
                    <a href="tel:{{$phone_number_f}}"><span>F</span>{{$phone_number_f}}</a>
                </p>
            @endif
            @if ($social_media_accounts)
                @php
                    $title = get_field('title', 'option');
                    $linkedin = get_field('linkedin', 'option');
                    $instagram = get_field('instagram', 'option');
                    $facebook = get_field('facebook', 'option');
                    $twitter = get_field('twitter', 'option');
                @endphp
                <div class="c-contact-info__social-media">
                    <p>{{$title}}</p>
                    @if (!empty($linkedin))
                        <a class="c-contact-info__link" href="{{$linkedin}}" target="_blank">
                            <svg version="1.1" id="linkedin" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="36px" height="36px" viewBox="0 0 430.117 430.118" style="enable-background:new 0 0 430.117 430.118;" xml:space="preserve">
                                <g>
                                    <path id="LinkedIn__x28_alt_x29_" d="M398.355,0H31.782C14.229,0,0.002,13.793,0.002,30.817v368.471
                                        c0,17.025,14.232,30.83,31.78,30.83h366.573c17.549,0,31.76-13.814,31.76-30.83V30.817C430.115,13.798,415.904,0,398.355,0z
                                         M130.4,360.038H65.413V165.845H130.4V360.038z M97.913,139.315h-0.437c-21.793,0-35.92-14.904-35.92-33.563
                                        c0-19.035,14.542-33.535,36.767-33.535c22.227,0,35.899,14.496,36.331,33.535C134.654,124.415,120.555,139.315,97.913,139.315z
                                         M364.659,360.038h-64.966V256.138c0-26.107-9.413-43.921-32.907-43.921c-17.973,0-28.642,12.018-33.327,23.621
                                        c-1.736,4.144-2.166,9.94-2.166,15.728v108.468h-64.954c0,0,0.85-175.979,0-194.192h64.964v27.531
                                        c8.624-13.229,24.035-32.1,58.534-32.1c42.76,0,74.822,27.739,74.822,87.414V360.038z M230.883,193.99
                                        c0.111-0.182,0.266-0.401,0.42-0.614v0.614H230.883z"/>
                                </g>
                            </svg>
                        </a>
                    @endif
                    @if (!empty($instagram))
                        <a class="c-contact-info__link" href="{{$instagram}}" target="_blank">
                            <svg version="1.1" id="instagram" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="36px" height="36px" viewBox="0 0 486.392 486.392" style="enable-background:new 0 0 486.392 486.392;" xml:space="preserve">
                                <g>
                                    <path d="M464.413,0H21.948C9.849,0,0,9.819,0,21.918v442.555c0,12.069,9.849,21.918,21.948,21.918h442.464
                                    c12.129,0,21.979-9.849,21.979-21.918V21.918C486.392,9.819,476.542,0,464.413,0z M243.196,158.32
                                    c46.876,0,84.875,37.999,84.875,84.875s-37.999,84.875-84.875,84.875s-84.875-37.999-84.875-84.875S196.32,158.32,243.196,158.32z
                                     M425.593,425.593H60.799V212.796h40.036c-2.098,9.819-3.314,19.972-3.314,30.399c0,80.346,65.359,145.674,145.674,145.674
                                    c80.346,0,145.674-65.328,145.674-145.674c0-10.427-1.216-20.58-3.283-30.399h40.006V425.593z M425.593,151.997h-91.198V60.799
                                    h91.198V151.997z"/>
                                </g>
                            </svg>
                        </a>
                    @endif
                    @if (!empty($facebook))
                        <a class="c-contact-info__link" href="{{$facebook}}" target="_blank">
                            <svg version="1.1" id="facebook" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="36px" height="36px" viewBox="0 0 96.124 96.123" style="enable-background:new 0 0 96.124 96.123;" xml:space="preserve">
                                <g>
                                    <path d="M72.089,0.02L59.624,0C45.62,0,36.57,9.285,36.57,23.656v10.907H24.037c-1.083,0-1.96,0.878-1.96,1.961v15.803
                                        c0,1.083,0.878,1.96,1.96,1.96h12.533v39.876c0,1.083,0.877,1.96,1.96,1.96h16.352c1.083,0,1.96-0.878,1.96-1.96V54.287h14.654
                                        c1.083,0,1.96-0.877,1.96-1.96l0.006-15.803c0-0.52-0.207-1.018-0.574-1.386c-0.367-0.368-0.867-0.575-1.387-0.575H56.842v-9.246
                                        c0-4.444,1.059-6.7,6.848-6.7l8.397-0.003c1.082,0,1.959-0.878,1.959-1.96V1.98C74.046,0.899,73.17,0.022,72.089,0.02z"/>
                                </g>
                            </svg>
                        </a>
                    @endif
                    @if (!empty($twitter))
                        <a class="c-contact-info__link" href="{{$twitter}}" target="_blank">
                            <svg version="1.1" id="twitter" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="36px" height="36px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve">
                                <g>
                                    <path  d="M612,116.258c-22.525,9.981-46.694,16.75-72.088,19.772c25.929-15.527,45.777-40.155,55.184-69.411
                                    c-24.322,14.379-51.169,24.82-79.775,30.48c-22.907-24.437-55.49-39.658-91.63-39.658c-69.334,0-125.551,56.217-125.551,125.513
                                    c0,9.828,1.109,19.427,3.251,28.606C197.065,206.32,104.556,156.337,42.641,80.386c-10.823,18.51-16.98,40.078-16.98,63.101
                                    c0,43.559,22.181,81.993,55.835,104.479c-20.575-0.688-39.926-6.348-56.867-15.756v1.568c0,60.806,43.291,111.554,100.693,123.104
                                    c-10.517,2.83-21.607,4.398-33.08,4.398c-8.107,0-15.947-0.803-23.634-2.333c15.985,49.907,62.336,86.199,117.253,87.194
                                    c-42.947,33.654-97.099,53.655-155.916,53.655c-10.134,0-20.116-0.612-29.944-1.721c55.567,35.681,121.536,56.485,192.438,56.485
                                    c230.948,0,357.188-191.291,357.188-357.188l-0.421-16.253C573.872,163.526,595.211,141.422,612,116.258z"/>
                                </g>
                            </svg>
                        </a>
                    @endif
                </div>
            @endif
            @if (have_rows('employee_card'))
                @while (have_rows('employee_card'))
                    @php
                        the_row();
                        $employee_image = App::getImageSrc(get_sub_field('employee_image'), 'thumbnail');
                        $text_headline = get_sub_field('text_headline');
                        $button_protocol = get_sub_field('button_protocol');
                        $linked_button = get_sub_field('linked_button');
                        $button_text = get_sub_field('button_text');
                        $button_href = $button_protocol . ':' . get_sub_field('button_href');
                    @endphp
                    <div class="c-employee-card">
                        <div class="c-employee-card__image" style="background-image: url({{$employee_image}});">

                        </div>
                        <div class="c-employee-card__info">
                            <p class="c-employee-card__text">{{$text_headline}}</p>
                            @if ($button_protocol == 'simple')
                                <a href="{{$linked_button['url']}}" target="{{$linked_button['target']}}" class="c-button "><span>{{$linked_button['text']}}</span></a>
                            @else
                                <a href="{{$button_href}}" class="c-button "><span>{{$button_text}}</span></a>
                            @endif
                        </div>
                    </div>
                @endwhile
            @endif
        </div>
    </div>
    <div class="col-xs-12 col-md-5" data-slide-up>
        <div class="c-form-wrap p-t-40 p-x-20 p-x-md-60" data-duration-01s>
            <h2>{{$heading_right}}</h2>
            <div data-editor>@php echo $text_content; @endphp</div>
	        <div class="c-form">
                @php gravity_form($form['id'], false, false, false, '', true, 12); @endphp
            </div>
        </div>
    </div>
</div>
