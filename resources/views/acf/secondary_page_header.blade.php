@php
    $background = get_sub_field('background_type') == 'image' ? 'background-image: url(' . App::getImageSrc(get_sub_field('background_image')) . ')' : 'background-color:' . get_sub_field('background_color');
    $breadcrumb_link = get_sub_field('breadcrumb_link');
    $title = !empty(get_sub_field('title')) ? get_sub_field('title') : App::title();
    $description = get_sub_field('description');
    $title_color = get_sub_field('title_color');
    $description_color = get_sub_field('description_color');
    $breadcrumb_link = get_sub_field('breadcrumb_link');
    $sab_title = get_sub_field('sab_title');
@endphp

<div class="l-page-header" data-waypoint>
    <div class="l-page-header__inner" style="{{$background}}" data-fade-in>
        <div class="l-page-header__section" data-{{$title_color}} data-slide-up>
            <div class="c-header-content" data-duration-01s>
                @if ( $breadcrumb_link )
                    @if ( !empty($post->post_parent) )
                        @php $parent_id = $post->post_parent; @endphp
                        <div class="c-header-content__breadcrumbs">
                            <a class="c-header-content__breadcrumb" href="{{get_the_permalink($parent_id)}}">back to {{get_the_title($parent_id)}}</a>
                        </div>
                    @endif
                @else
                    @if ( !empty($sab_title) )
                        <div class="c-header-content__section ">{{$sab_title}}</div>
                    @endif
                @endif
                <h1 class="c-header-content__title">{{ $title }}</h1>
            </div>
        </div>
        @if ( !empty($description) )
            <div class="l-page-header__section" data-{{$description_color}} data-slide-up>
                <div class="c-header-content" data-duration-02s>
                    <div class="c-header-content__description">{{$description}}</div>
                </div>
            </div>
        @endif
    </div>
</div>


