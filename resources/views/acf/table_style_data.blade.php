@php $count = 0; @endphp
<div class="l-table">
    @if (have_rows('columns'))
        @while (have_rows('columns'))
            @php
                the_row();
                $title = get_sub_field('column_title');
                $column_content = get_sub_field('column_content');
            @endphp
            <div class="l-table__column" data-waypoint>
                <div class="c-column-content" data-slide-up>
                    <div class="c-column-content__title-background"
                         data-duration-<?php echo $count == 0 ? $count : '0' . $count; ?>s>
                        <h3 class="c-column-content__title">{{$title}}</h3>
                    </div>
                    <div class="c-column-content__content"
                         data-editor
                         data-duration-<?php echo $count == 0 ? $count : '0' . $count; ?>s>
                        @php echo $column_content; @endphp
                    </div>
                </div>
            </div>
            @php $count == 4 ? $count = 0 : $count++; @endphp
        @endwhile
    @endif
</div>