@php
    $blocks = get_sub_field('blocks');
    $size = count($blocks) == 3 ? 4 : 6;
@endphp
<div class="l-container l-container_lg" data-waypoint>
    <div class="row p-t-20 p-b-20 p-t-sm-30 p-b-sm-30" data-slide-up>
        @if ( isset($blocks) && count($blocks) > 2 )
            @foreach ($blocks as $count_item => $block)
                @php  
                    $title = get_the_title($block->ID);
                    $excerpt = get_post_meta($block->ID, 'excerpt_content', true);
                    $url = get_the_permalink($block->ID);
                    $image_url = false;
                    
                    if ( strlen($excerpt) > 79 ) {
                        $excerpt = substr($excerpt, 0, 76) . '...';
                    }
                    
                    if ( get_field('featured_image', $block->ID) ) {
                        $image = get_field('featured_image', $block->ID);                
                        $image_url = $image['sizes']['medium_large'];
                    } 
                                    
                @endphp
                <div class="p-x-10 p-t-10 p-b-10 p-x-sm-20 p-t-sm-10 p-b-sm-10 col-xs-12 col-md-{{$size}}" data-duration-<?php echo $count_item == 0 ? 0 : '0' . $count_item; ?>s>
                    <div class="c-linked-block " style="background-image: url({{$image_url}})">
                        <div class="c-linked-block__content">
                            <h3 class="c-linked-block__title">{{$title}}</h3>
                            <p class="с-linked-block__text">{{$excerpt}}</p>
                            <div class="c-linked-block__button">
                                <a class="c-button" href="{{$url}}"><span>Learn more</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
</div>