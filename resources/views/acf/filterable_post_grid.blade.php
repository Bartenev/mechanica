@php
    $active_option = get_sub_field('active_option');
    $array_filters = array(
        'featured' => 'Featured',        
        'industry' => 'Clients',
        'challenges' => 'Challenges'
    );
    $count_filter = 0;
    $industry_categories = get_terms('industry');
@endphp
<div class="l-container l-container_fluid l-container_fluid_content" data-waypoint>
    <ul class="c-filter-bar c-filter-bar_work" data-slide-up>
        <li data-dropdown="true">Filter by: <span>{!! $array_filters[$active_option] !!}</span></li>
        <li data-duration-{{$count_filter}}s><span data-filter="{{$active_option}}"
                                                   data-filter-type="{{$active_option}}"
                                                   class="active">{!! $array_filters[$active_option] !!}</span>
            @if ($active_option == 'industry')
                <div class="c-sub-filter-bar">
                    <div>
                        <ul>
                            @foreach ($industry_categories as $key => $category)
                                <li><span data-filter="{{$category->slug}}">{{strtoupper($category->name)}}</span></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
        </li>
        @foreach($array_filters as $key => $filter)
            @php $count_filter++; @endphp
            @if ( $key !== $active_option )
                <li data-duration-0{{$count_filter}}s>
                    <span data-filter="{{$key}}" data-filter-type="{{$key}}">{{$filter}}</span>
                    @if ($key == 'industry')
                        <div class="c-sub-filter-bar">
                            <div>
                                <ul>
                                    @foreach ($industry_categories as $key => $category)
                                        <li><span data-filter="{{$category->slug}}">{{strtoupper($category->name)}}</span></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                </li>
            @endif
        @endforeach
    </ul>
</div>

@php
    $args = array(
        'post_type' => array('case'),    
        'orderby' => 'menu_order',  
        'order' => 'ASC',
        'posts_per_page' => 8,
    );

    if ( $active_option == 'featured' ) {
        $args['meta_query'][] =  array(
            'key' => 'featured_post',
            'value' => true,
            'compare' => '='
        );
    }

    if ( $active_option == 'industry' ) {
        $category_array = array();

        foreach ($industry_categories as $category) {
            array_push($category_array, $category->slug);
        }

        $args['tax_query'] = array(
            'relation' => 'OR',
            array(
                'taxonomy' => 'industry',
                'field'    => 'slug',
                'terms'    => $category_array,
            )
        );
    }

    if ( $active_option == 'challenges' ) {
        $args['post_type'] = array('challenges');
    }

    $query = new WP_Query( $args );
    $count_post = 0;
    $count_animation = 0;
@endphp

@if ( $query->have_posts() )
    <div class="l-wrap">
        <div class="c-sub-filter-bar">
            @if ($active_option == 'industry')
                <div data-waypoint>
                    <ul data-slide-up>
                        @foreach ($industry_categories as $key => $category)
                            <li data-duration-0{{$key}}s>{!! $key != 0 ? '<i>|</i>' : ''; !!}<span data-filter="{{$category->slug}}">{{strtoupper($category->name)}}</span></li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
        <div class="l-4-col"
             data-filter="work"
             data-container-ajax
             data-post-type="{{$active_option}}"
             data-max-page="{{$query->max_num_pages}}">
            @while ( $query->have_posts() )
                @php
                    $query->the_post();
                    $id = get_the_id();
                    $post_excerpt = get_post_meta($id, 'excerpt')[0];
                    if ( strlen($post_excerpt) > 222 ) {
                        $post_excerpt = substr($post_excerpt, 0, 198) . '...';
                    }
                    $bg_image_id = get_post_type() == 'case' ? get_post_meta($id, 'image')[0] : get_post_meta($id, 'background_image')[0];
                    $bg_image = 'background-image: url(' . App::getImageSrc($bg_image_id, 'medium_large') . ');';
                    $bg_type = get_post_meta($id, 'background_type')[0];
                    $select_type = get_post_meta($id, 'select_type')[0];
                    $logo = get_post_meta($id, 'select_type')[0]  == 'image' ? App::getImageSrc(get_post_meta($id, 'logo_image_grid')[0], 'medium_large') : '';
					$text = get_post_meta($id, 'select_type')[0]  == 'text' ? get_post_meta($id, 'text')[0] : '';
                    $select_type = get_post_meta($id, 'select_type')[0] == 'image' ? 'style="background-image: url(' . $logo .')"' : 'data-text';
                    $button_text = $active_option == 'challenges' ? 'Learn More' : 'SEE THE WORK';
                @endphp
                <div class="l-4-col__item" data-waypoint>
                    <div class="c-filterable-post" data-type="{{$active_option}}" data-color-{!! $bg_type == 'color' || $type === 'challenges' ? get_post_meta($id, 'background_color')[0] : '';!!} data-slide-up>
                        <div class="c-filterable-post__inner"
                             style="{{$bg_image}}"
                             data-duration-@php echo $count_animation == 0 ? 0 : '0' . $count_animation . 's'; @endphp
                        >
                            <div class="c-filterable-post__logo" {!! $select_type !!}><span>{{$text}}</span></div>
                            <div class="c-filterable-post__content">
                                @if ( $post_excerpt )
                                    <p>{{$post_excerpt}}</p>
                                @endif                                
                                <a class="c-button" href="{{get_the_permalink()}}"><span>{{$button_text}}</span></a>
                            </div>
                            @if ($active_option !== 'challenges' && $bg_type == 'image')
                                <svg class="c-filterable-post__image-blur">
                                    <defs>
                                        <filter id="blur-effect-{{$count_post}}" class="blur-effect">
                                            <feGaussianBlur stdDeviation="2"/>
                                        </filter>
                                    </defs>
                                    <image class="svg-image" xlink:href="{{App::getImageSrc($bg_image_id)}}" width="100%" height="100%" />
                                    <image class="svg-image svg-image_blur" width="100%" height="100%"
                                           xlink:href="{{App::getImageSrc($bg_image_id)}}"
                                           filter="url(#blur-effect-{{$count_post}})"/>
                                </svg>
                                <div class="c-filterable-post__image" style="background-image: url({{App::getImageSrc($bg_image_id)}});"></div>
                                <image class="c-filterable-post__image-size" src="{{App::getImageSrc($bg_image_id)}}" />
                            @else
                                @php $secondary_background_image = App::getImageSrc(get_post_meta($id, 'secondary_background_image')[0], 'medium_large'); @endphp
                                <div class="c-filterable-post__background" style="{{$bg_image}}"></div>
                                <div class="c-filterable-post__background-hover" style="background-image: url({{$secondary_background_image}});"></div>
                            @endif
                        </div>
                    </div>
                </div>
                @php
                    $count_post++;
                    $count_animation == 3 ? $count_animation = 0 : $count_animation++;
                @endphp
            @endwhile
        </div>
    </div>
@endif
@php wp_reset_query(); @endphp
