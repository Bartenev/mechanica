@php

    $show_hide_title_bar = get_sub_field('show_hide_title_bar');
    $custom_title_or_icon = get_sub_field('custom_title_or_icon');
    $custom_title = $custom_title_or_icon && !empty(get_sub_field('custom_title')) ? get_sub_field('custom_title') : 'FOLLOW @MECHANICAUSA';
    $custom_image_icon = $custom_title_or_icon && !empty(get_sub_field('custom_image_icon')) ? 'background-image: url(' . App::getImageSrc(get_sub_field('custom_image_icon'), 'thumbnail') . ')' : '';
    $linked_button=  get_sub_field('linked_button');
    $client_id = get_sub_field('client_id');
    $access_token = get_sub_field('access_token');
    $instagram_account = get_field( 'instagram', 'option' );
@endphp
@if ( !empty($client_id) && !empty($access_token) )
    <div class="l-wrap" data-waypoint>
        <div class="c-instagram" data-slide-up>
            @if ( $show_hide_title_bar )
                <div class="c-instagram__title">
                    <a href="{{$instagram_account}}" target="_blank">
                        <h3>
                            {{$custom_title}}
                            <span style="{{$custom_image_icon}}"></span>
                        </h3>
                    </a>
                    <a href="{{$linked_button['url']}}" target="{{$linked_button['target']}}" class="c-button c-button_extra-large"><span>{{$linked_button['text']}}</span></a>
                </div>
            @endif
            <ul class="c-instagram__grid" data-client-id="{{$client_id}}" data-accetss-token="{{$access_token}}"></ul>
        </div>
    </div>
@endif
