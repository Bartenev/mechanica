@php
    $background_style = get_sub_field('background_style');
    $background = $background_style == 'image' ? 'background-image: url(' . App::getImageSrc(get_sub_field('background_image')) . ')' : 'background-color:' . get_sub_field('background_color');
    $text_content = get_sub_field('text_content');
@endphp
<div class="l-content-center" data-waypoint>
    <div class="l-content-center__inner" style="{{$background}}" data-slide-up>
        <div class="c-text-content" data-editor="white">
            @php echo $text_content @endphp
        </div>
    </div>
</div>