@php
    $header_content = get_sub_field('header_content');
    $footer_text = get_sub_field('footer_text');
    $linked_button = get_sub_field('linked_button');
@endphp
<div class="c-tabbed-wrap">
    <div class="c-tabbed-wrap__overlay" data-waypoint>
        <div data-fade-in></div>
    </div>
    <div class="l-container">
        <div class="row row_center">
            <div class="col-12 p-t-60 p-t-lg-120" >
                <div class="c-tabbed-header"
                     data-editor
                     data-waypoint>
                    <div data-slide-up>{!! $header_content !!}</div>
                </div>
            </div>
            @php $count_item = count(get_sub_field('tabbed_content')); @endphp
            @if (have_rows('tabbed_content'))

                <div class="c-tabbed" data-waypoint data-count="{{$count_item}}">
                    <ul class="c-tabbed__list" data-slide-up>
                        @php
                            $count_trigger = 1;

                        @endphp
                        @while (have_rows('tabbed_content'))
                            @php
                                the_row();
                                $tab_name = get_sub_field('tab_name');
                                $section_title = get_sub_field('section_title');
                                $number_of_columns_content = get_sub_field('number_of_columns_content');
                                $column_content_one = get_sub_field('column_content_one');
                            @endphp
                            <li class="c-tabbed__trigger @php echo $count_trigger == 1 ? 'active' : ''; @endphp" data-index="{{$count_trigger - 1}}">
                                <h3>{{$count_trigger}} <span>{{$tab_name}}</span></h3>
                                <div class="c-tabbed__content">
                                    <div class="c-tabbed__item @php echo $count_content == 1 ? 'active' : ''; @endphp">
                                        <h3><span>{{$section_title}}</span></h3>
                                        @if ( $number_of_columns_content == 'one' )
                                            <ul>
                                                <li>
                                                    {!! $column_content_one !!}
                                                    @if (have_rows('download_link'))
                                                        @while(have_rows('download_link'))
                                                            @php
                                                                the_row();
                                                                $icon = App::getImageSrc(get_sub_field('icon'), 'thumbnail');;
                                                                $link_text = get_sub_field('link_text');
                                                                $download_file = get_sub_field('download_file');
                                                            @endphp
                                                            <a href="{{wp_get_attachment_url($download_file['id'])}}" class="c-tabbed__link" download>
                                                                <span style="background-image: url({{$icon}});"></span>
                                                                {{$link_text}}
                                                            </a>
                                                        @endwhile
                                                    @endif
                                                </li>
                                            </ul>
                                        @else
                                            @if (have_rows('column_content_reputer'))
                                                <ul>
                                                    @while(have_rows('column_content_reputer'))
                                                        @php the_row(); @endphp
                                                        <li>
                                                            {!! get_sub_field('column_content') !!}
                                                            @if (have_rows('download_link_two'))
                                                                @while(have_rows('download_link_two'))
                                                                    @php
                                                                        the_row();
                                                                        $icon = App::getImageSrc(get_sub_field('icon'), 'thumbnail');;
                                                                        $link_text = get_sub_field('link_text');
                                                                        $download_file = get_sub_field('download_file');
                                                                    @endphp
                                                                    <a href="{{wp_get_attachment_url($download_file['id'])}}" class="c-tabbed__link" download>
                                                                        <span style="background-image: url({{$icon}});"></span>
                                                                        {{$link_text}}
                                                                    </a>
                                                                @endwhile
                                                            @endif
                                                        </li>
                                                    @endwhile
                                                </ul>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            </li>
                            @php $count_trigger++; @endphp
                        @endwhile
                    </ul>
                </div>
            @endif
            <div class="col-12" data-waypoint>
                <div class="c-tabbed-footer" data-slide-up>
                    @if ( !empty($footer_text))
                        <p>{{$footer_text}}</p>
                    @endif
                    @if ( !empty($linked_button))
                        <a href="{{$linked_button['url']}}" target="{{$linked_button['target']}}" class="c-button c-button_large">{{$linked_button['text']}}</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>