@php
    $count_cols = count(get_sub_field('columns'));
    $count_item = 0;
@endphp

<div class="l-wrap" data-waypoint data-three-column-images>
    <div class="row row_gutter" data-slide-up>
        @while (have_rows('columns'))
            @php
                the_row();
                $image = wp_get_attachment_image_src(get_sub_field('image'), 'large');
                $image_src = $image[0];
                $image_width = $image[1];
                $image_height = $image[2];
                $count_rows = 3;
            @endphp
            <div class="col-xs-12 col-sm-{{12 / $count_cols}}" data-duration-<?php echo $count_item == 0 ? 0 : '0' . $count_item; ?>s>
                @if ($count_rows > 1)
                    <div class="row {{ $count_rows == 1 ? 'row_gutter' : '' }}">
                        <div class="col-xs-12">
                            <div class="c-image-card" data-width="{{$image_width}}" data-height="{{$image_height}}" style="background-image: url({{$image_src}});" ></div>
                        </div>
                    </div>
                @else
                    <div class="c-image-card" data-width="{{$image_width}}" data-height="{{$image_height}}" style="background-image: url({{$image_src}});" ></div>
                @endif
            </div>
            @php $count_item++; @endphp
        @endwhile
    </div>
</div>
