@php
    $text_content = get_sub_field('text_content');
    $button_align = get_sub_field('button_align');
    $linked_button = get_sub_field('linked_button');
@endphp
<div class="l-container p-t-50 p-b-60 p-t-sm-100 p-b-sm-100" data-waypoint>
    <div class="c-callout" data-editor data-slide-up>
        <div data-duration-0s>
            @php echo $text_content; @endphp
        </div>
        <div class="c-callout__button c-callout__button_{{$button_align}}" data-duration-01s>
            <a href="{{$linked_button['url']}}" target="{{$linked_button['target']}}" class="c-button c-button_large"><span>{{$linked_button['text']}}</span></a>
        </div>
    </div>
</div>