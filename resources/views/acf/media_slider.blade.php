@php
    $logo = App::getImageSrc(get_sub_field('Logo'));
    $title = !empty(get_sub_field('title')) ? get_sub_field('title') : App::title();
@endphp
<div class="l-viewport">
    <div class="c-media-swiper" data-slider="media-slider" data-waypoint>
        <div class="c-media-swiper__inner">
            @if (have_rows('slide'))
                @while ( have_rows('slide'))
                    @php
                        the_row();
                        $video_url = get_sub_field('slide_type') == 'video' ? get_sub_field('video_url') : '';
                        $slider_image = App::getImageSrc(get_sub_field('image'));
                    @endphp
                    <div class="c-media-swiper__slide" style="background-image: url({{$slider_image}});" data-fade-in>
                        @if ( !empty($video_url) )
                            <button class="c-btn-play"
                                    data-lightbox-trigger
                                    data-lightbox-media="{{$video_url}}">
                            </button>
                        @endif
                    </div>
                @endwhile
            @endif
        </div>
        <div class="c-media-swiper__button c-media-swiper__button_prev" data-slide-up>
            <svg width="23px" height="31px">
                <path fill-rule="evenodd"  fill="rgb(218, 40, 28)" d="M22.994,0.505 L7.446,15.499 L22.994,30.495 L15.556,30.495 L0.006,15.499 L15.556,0.505 L22.994,0.505 Z"/>
            </svg>
        </div>
        <div class="c-media-swiper__button c-media-swiper__button_next" data-slide-up>
            <svg width="23px" height="31px">
                <path fill-rule="evenodd"  fill="rgb(218, 40, 28)" d="M22.994,0.505 L7.446,15.499 L22.994,30.495 L15.556,30.495 L0.006,15.499 L15.556,0.505 L22.994,0.505 Z"/>
            </svg>
        </div>
        <div class="c-media-swiper__footer" data-slide-up>
            <span class="c-button c-button_scroll" data-scroll-trigger data-duration-03s></span>
            <div class="c-media-swiper__content c-media-swiper__content_between">
                <span class="c-media-swiper__logo" data-duration-01s  @if (strlen($logo)) style="background-image: url({{$logo}});" @endif></span>
                <div class="c-media-swiper__content c-media-swiper__content_between c-media-swiper__content_right" data-duration-02s>
                    <h1 class="c-media-swiper__title">{{$title}}</h1>
                    <ul class="c-media-swiper__pagination"></ul>
                </div>
            </div>
        </div>
    </div>
</div>
