@php
    if ( get_sub_field( 'logo_image' ) ) {
        $logo_image = get_sub_field( 'logo_image' );
        $logo_image_id = $logo_image['ID'];
        $logo_image_url = $logo_image['sizes']['medium_large'];
        $logo_image_alt = App::getImageSrc($logo_image_id);
    }
    $text_content = get_sub_field( 'text_content' );
    $featured_image = get_sub_field( 'featured_image' );
    $featured_image_url = $featured_image['sizes']['medium_large'];
    $featured_image_size = get_sub_field('featured_image_size');
    $linked_button =  get_sub_field('linked_button');
    $row_layout = get_sub_field( 'row_layout' );
@endphp
<div class="l-wrap">
    <div class="c-case-studies" data-waypoint>
        <div class="c-case-studies__inner @php echo 'layout-' . $row_layout; @endphp" data-slide-up>
            <div class="c-case-studies__photo" style="background-image: url({{$featured_image_url}}); background-size: {{$featured_image_size}};" data-duration-0s></div>
            <div class="c-case-studies__about" data-duration-01s>
                @if ( isset($logo_image) && ! empty( $logo_image ) )
                    <img class="c-case-studies__logo" src="{{$logo_image_url}}" alt="{{$logo_image_alt}}">
                @endif
                <div class="c-case-studies__description" data-editor>@php echo $text_content; @endphp</div>
                <div class="c-case-studies__button">
                    @if ( $linked_button )
                        <a href="{{ $linked_button['url']}}" target="{{$linked_button['target']}}" class="c-button">
                            {{ $linked_button['text'] }}
                        </a>
                    @endif
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
