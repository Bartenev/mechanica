@php
    $args = array(
        'post_type' => 'team_members',
        'post_date' => 'DESC',
        'posts_per_page' => -1
    );

    $query = new WP_Query( $args );
    $count = 0;

@endphp
@if ( $query->have_posts() )
<div class="l-wrap m-t-40">
    <div class="row row_gutter-20"
         data-post-type="team_members"
         data-container-ajax
         data-max-page="{{$query->max_num_pages}}">
        @while ( $query->have_posts() )
            @php
                $query->the_post();
                $id = get_the_id();
                $professional_title = get_post_meta($id, 'professional_title')[0];
                $post_content = get_post_meta($id, 'content', true);                                
                $featured_image = App::getImageSrc(get_post_meta($id, 'image')[0], 'medium_large');
                $title = !empty(get_post_meta($id, 'first_name')[0]) && !empty(get_post_meta($id, 'last_name')[0]) ? get_post_meta($id, 'first_name')[0] . ' ' . get_post_meta($id, 'last_name')[0] : get_post_title();
                $first_name = !empty(get_post_meta($id, 'first_name')[0]) ? get_post_meta($id, 'first_name')[0] : get_post_title();
                $email = get_post_meta($id, 'contact_email')[0];
                $phone = get_post_meta($id, 'contact_phone')[0];
                $posts = get_post_meta($id, 'posts')[0];
                $image_position = get_post_meta($id, 'image_position')[0];
            @endphp
            <div class="col-xs-12 col-sm-4" data-waypoint>
                <div data-slide-up>
                    <div class="c-team-row c-team-row_{{$image_position}} lazyload"
                         data-duration-<?php echo $count == 0 ? 0  : '0' . $count; ?>s
                         data-swipe-panel-trigger
                         style="background-image: url(@asset('images/default-bg.jpg'));"
                         data-image="{{$featured_image}}"
                         data-src="{{$featured_image}}">
                        <span class="c-team-row__arrow">
                            <svg xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                 width="24px" height="31px">
                                <path fill-rule="evenodd"  fill="rgb(255, 255, 255)" d="M0.506,30.094 L16.054,15.098 L0.506,0.099 L7.944,0.099 L23.494,15.098 L7.944,30.094 L0.506,30.094 Z"/>
                            </svg>
                        </span>
                        <div class="c-team-row__info" data-first-name="{{$first_name}}" data-email="{{$email}}" data-phone="{{$phone}}">
                            <h2>
                                {{$title}}
                                <span>{{$professional_title}}</span>
                            </h2>
                            <div class="c-team-row__content">
                                <?php echo apply_filters( 'the_content', $post_content ); ?>
                            </div>
                            @if ( !empty($posts) )
                                <ul class="c-team-row__post">
                                    <li>Recent thoughts from {{$first_name}}</li>
                                    @foreach ($posts as $post)
                                        <li><a href="{{get_the_permalink($post)}}">{{get_the_title($post)}}</a></li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            @php $count == 2 ? $count = 0 : $count++; @endphp
        @endwhile
    </div>
    <div class="с-loader-ellipse">
        <span class="с-loader-ellipse__dot"></span>
        <span class="с-loader-ellipse__dot"></span>
        <span class="с-loader-ellipse__dot"></span>
        <span class="с-loader-ellipse__dot"></span>
    </div>
</div>
@endif
@php wp_reset_query(); @endphp
