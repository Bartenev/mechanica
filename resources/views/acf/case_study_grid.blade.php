@php
    $choose = get_sub_field('choose');
    $case_studies = get_sub_field('case_studies');
    $pain_point = get_sub_field('pain_point');
@endphp
<div class="l-wrap">
    @if ( $choose == 'manually' )
        @foreach ($case_studies as $case)
            @php
                $image = App::getImageSrc(get_post_meta($case->ID, 'image')[0], 'medium_large');
                $logo_image_id = get_post_meta($case->ID, 'logo_image')[0];
                $logo_image_alt = App::getImageAlt($logo_image_id);
                $logo_image_url = App::getImageSrc($logo_image_id, 'medium_large');
                $linked_button = '<a href="' . get_the_permalink($case->ID) . '" class="c-button"><span>VIEW MORE WORK</span></a>';
            @endphp
            <div class="c-case-studies" data-waypoint>
                <div class="c-case-studies__inner" data-slide-up>
                    <div class="c-case-studies__photo" style="background-image: url({{$image}})" data-duration-0s></div>
                    <div class="c-case-studies__about" data-duration-01s>
                        <img class="c-case-studies__logo" src="{{$logo_image_url}}" alt="{{ $logo_image_alt }}">
                        <div class="c-case-studies__description" data-editor>@php echo the_field('description', $case->ID) @endphp</div>
                        <div class="c-case-studies__button">@php echo $linked_button @endphp</div>
                    </div>
                </div>
            </div>
        @endforeach
        @php wp_reset_postdata(); @endphp
    @else
        @php
            $args = array(
                'post_type' => 'case',
                'post_pre_page' => -1,
                'post_date' => 'DESC',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'challenges',
				        'field' => 'id',
				        'terms' => $pain_point
				    )
                )
            );
            $query = new WP_Query( $args );
        @endphp
        @if ( $query->have_posts())
            @while ($query->have_posts())
                @php
                    $query->the_post();
                    $id = get_the_id();
                    $image = App::getImageSrc(get_post_meta($id, 'image')[0]);
                    $logo_image_id = get_post_meta($id, 'logo_image')[0];
                    $logo_image_alt = App::getImageAlt($logo_image_id);
                    $logo_image_url = App::getImageSrc($logo_image_id);
                    $linked_button = '<a href="' . get_the_permalink($id) . '" class="c-button"><span>VIEW MORE WORK</span></a>';
                @endphp
                <div class="c-case-studies" data-waypoint>
                    <div class="c-case-studies__inner" data-slide-up>
                        <div class="c-case-studies__photo" style="background-image: url({{$image}})" data-duration-0s></div>
                        <div class="c-case-studies__about" data-duration-01s>
                            <img class="c-case-studies__logo" src="{{$logo_image_url}}" alt="{{$logo_image_alt}}">
                            <div class="c-case-studies__description" data-editor>@php echo the_field('description', $id); @endphp</div>
                            <div class="c-case-studies__button">@php echo $linked_button @endphp</div>
                        </div>
                    </div>
                </div>
            @endwhile
        @endif
        @php wp_reset_query(); @endphp
    @endif
</div>
